﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Description of the Maint. LOS Analysis Window</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Description of the Maint. LOS Analysis Window</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Maintenance LOS Analysis window contains two panes: &#160;the Results by Year pane and the Summarized Results pane. These two panes are described in more detail in the following sections.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Above these panes are the input parameters for the analysis engine along with two command buttons. These are described below:</text></para>
    <list id="134" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Desired Level of Service — This field contains a drop-down list. The items in the list are the various condition states (levels-of-service) that may be specified as the desired condition of the selected element. The system uses the selected condition state to determine the resource requirements (in dollars) to achieve the desired state.</text></li>
    </list>
    <list id="135" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Element Type — The field contains a drop-down list. The items in the list are those structural elements that may be analyzed. The list also includes an </text><text styleclass="Text As Shown on Screen" translate="true">All</text><text styleclass="Bullet L1" translate="true"> entry that allows you to analyze all structural elements.</text></li>
    </list>
    <list id="134" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Number of Years — In this type of analysis, the quantity of an element in each condition state (and consequently the maintenance needs associated with that element) changes every year. This is because, during a year, an element can deteriorate from a superior condition state to a lower condition state or can improve from it’s current state to a superior condition state (due to improvement treatments carried out on the element during the year). In the </text><text styleclass="Text As Shown on Screen" translate="true">Number of Years</text><text styleclass="Bullet L1" translate="true"> field, you specify the number of years (starting this year) for which you want to review an element’s maintenance needs. </text></li>
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Run Analysis</text><text styleclass="Bullet L1" translate="true"> button — Once the input parameters are set, you click this button to perform the analysis.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Show/Edit Parameters</text><text styleclass="Bullet L1 Last" translate="true"> button — This button displays the Show/Edit Parameters window. This window contains the other input parameters that are used in the maintenance analysis engine. </text></li>
    </list>
  </body>
</topic>
