﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Insert a New Record</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Insert a New Record</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">To add a new construction history section record, follow these steps:</text></para>
    <list id="2" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Display the Construction History window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Construction History Sections pane, right-click and then click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">. A new record is added to the pane.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Enter the road location information by selecting route, direction, and lane from the drop-down lists and by entering begin and end mile points in the appropriate columns.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Tab to the other columns and type the information if known.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true"> In the Sections Location pane, right-click and then click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">. A new record is added to the pane.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Complete the new record with the location where the construction work will be performed.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Repeat step 6 for any additional locations.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true"> In the Material Layer Information pane, define the layers placed or removed as part of the construction work. This may be accomplished by inserting a standard section (as described </text><link displaytype="text" defaultstyle="true" type="topiclink" href="InsertaStandardSection" styleclass="Bullet L1" translate="true">here</link><text styleclass="Bullet L1" translate="true">) or by right-clicking and then clicking </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true"> to add records to the pane. In each new record added via the </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true"> command:</text></li>
    </list>
    <list id="3" type="ol" listtype="lower-alpha" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Layer</text><text styleclass="Bullet L2" translate="true"> column, type the code for the layer.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Material Code</text><text styleclass="Bullet L2" translate="true"> column, select the material for the layer from the drop-down list accessed by clicking the down arrow. (Milling is treated as a layer and so a material code is needed.)</text></li>
      <li styleclass="Bullet L2 Last"><text styleclass="Bullet L2 Last" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Thickness</text><text styleclass="Bullet L2 Last" translate="true"> column, type the thickness of the layer. (Milling is entered as a negative value.)</text></li>
    </list>
    <list id="2" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click &#160;</text><image src="Icon_Save.png" scale="90.00%" width="19" height="19" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1" translate="true"> to save the new record.</text></li>
    </list>
  </body>
</topic>
