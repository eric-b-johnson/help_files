﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Setup Treatment Relationship Type</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Setup Treatment Relationship Type</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Setup Treatment Relationship Type window allows you to set the various treatment relationship types used during the &#160;automatic grouping process. It is recommended not to modify any of the defined statuses in this table.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">There are three predefined treatment relationship types:</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Type 1 — &#160;No relation. Treatment A has no relation to Treatment B.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Type 2 — Conflicting relation. Treatment A is marked &quot;invalid&quot; if Treatment B is performed X years before or Y years after Treatment A for a parameter % of overlapping location.</text></li>
    </list>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Type 3 — Must relationship. Treatment A should be performed with Treatment B if Treatment B is performed X years before or Y years after Treatment A for a parameter % of overlapping location.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The relationship between the treatments is defined in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Setup_Treatment_Relationships" domain="..\Setup Treatment Relationship\Setup Treatment Relationship.hmxp" styleclass="Body Text" translate="true">Treatment Relationships</link><text styleclass="Body Text" translate="true"> window.</text></para>
  </body>
</topic>
