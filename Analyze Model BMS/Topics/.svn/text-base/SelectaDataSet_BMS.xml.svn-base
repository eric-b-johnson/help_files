﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Select a Data Set for Analysis</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Select a Data Set for Analysis</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The node you select in the Decision Tree pane of the Performance Models window determines what data set will be used in analysis. The data set is that which fits the criteria of the selected node and its ancestors. (To see the criteria for a node, hover over the node and the system will display the decision criteria.)</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">To select a data set for analysis:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Display the Performance Models window.</text></li>
    </list>
    <para styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">2.</text><tab /><text styleclass="Bullet L1" translate="true">In the left pane, click the node that corresponds to the data set you wish to use in analysis.</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">To graph the data set, right-click the node and then click </text><text styleclass="Command" translate="true">Analyze Model</text><text styleclass="L1 Indent" translate="true">. The system displays the Analyze Model window that shows a condition versus age plot of all data points in the set. To determine the number of data points in the data set, click the Models tab. The </text><text styleclass="Text As Shown on Screen" translate="true">NUM SAMPLE</text><text styleclass="L1 Indent" translate="true"> column in the right pane identifies the number of points in the data set.</text></para>
  </body>
</topic>
