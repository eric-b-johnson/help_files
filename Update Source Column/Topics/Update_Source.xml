﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Update Source and Where Clause Columns</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Update Source and Where Clause Columns</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The definition of columns includes the option of using SQL statements to update the data in the column. These statements are placed in the </text><text styleclass="Text As Shown on Screen" translate="true">Update Source</text><text styleclass="Body Text" translate="true"> and </text><text styleclass="Text As Shown on Screen" translate="true">Where Clause</text><text styleclass="Body Text" translate="true"> fields. For the data to be successfully and correctly updated, you must complete these fields properly. &#160;</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The general form for the SQL statement is:</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">SELECT ( update_source ) AS column_name FROM table_name A WHERE where_clause</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Such that:</text></para>
    <list id="21" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The SELECT statement is legal and when run without the where_clause selects one and only one record for every record in the table_name.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The table_name is the appropriate table being updated (as identified in the currently selected record in the top pane of the window you’re using).</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The column_name is the appropriate column being updated (as identified in the currently selected record in the Columns pane of the window you’re using).</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The update_source is the text in the UPDATE_SOURCE field. If filled, then this record in &#160;the Columns pane is a calculated column.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The where_clause is the text in the WHERE_CLAUSE field. It need not be filled, but if filled, then the calculation is only applied to those records in the table that pass the where clause criteria.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: The best way to learn to write Update Source SQL is to find several existing examples and place them into the SELECT statement configuration shown above.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">To test if your update_source text is correct, place it into the SELECT statement configuration shown above without the where_clause. It should yield the same number of records as in the table with no duplication on the primary key or any unique index.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">To test if your where_clause text is correct, place it into the SELECT statement configuration shown above and see that it yields the expected filtered results.</text></para>
  </body>
</topic>
