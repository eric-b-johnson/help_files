﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Module Menu Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Module Menu Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">This is the right pane in the Access Level Settings window. For the currently selected module and access profile, it displays all of the menu items of the module in a tree view along with the access permission assigned to each item. (The access permission is shown to the left of the name of the menu item.)</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a menu item in the tree view, the system displays a shortcut menu with the following commands:</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Set This to the Branch</text><text styleclass="Bullet L1" translate="true"> — This command assigns the access level setting for the selected menu item to all sub-items.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Set Access Level</text><text styleclass="Bullet L1" translate="true"> — This command displays a new dialog box in which you may select the access level for the selected item. The following access levels are available:</text></li>
    </list>
    <list id="3" type="ul" listtype="bullet" formatstring="&#111;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">(0) Invisible – The user cannot see the menu item.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">(1) Disabled – The user can see the menu name, but cannot open the window.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">(2) Read Only – The user can see the window content, but cannot modify it.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">(3) Read/Write – The user can read and write data.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">(4) Accept/Confirm – The user can read and write data and has approval capability.</text></li>
      <li styleclass="Bullet L2 Last"><text styleclass="Bullet L2 Last" translate="true">(5) - (7) Super Users – The user has all the rights to edit data and grant approvals plus the additional rights defined in the Actions Rights and Columns windows.</text></li>
    </list>
  </body>
</topic>
