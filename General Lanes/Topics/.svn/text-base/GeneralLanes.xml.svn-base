﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Network Lanes</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Network Lanes</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Network Lanes window sets the number of lanes in each direction for your entire road network. Each record in this window indicates the number of lanes for a road section as identified by route, direction, start mile point, and end mile point. When entering data into this window, observe the following:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">For lane direction, </text><text styleclass="Bullet L1" style="font-weight:bold;" translate="true">never</text><text styleclass="Bullet L1" translate="true"> set the lane direction to &quot;All.&quot;</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">For lane ID#, </text><text styleclass="Bullet L1 Last" style="font-weight:bold;" translate="true">always</text><text styleclass="Bullet L1 Last" translate="true"> set the lane ID# to &quot;All.&quot;</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">This &quot;number of lanes&quot; information is used for finest partition operations in:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Finest Partition reports; and</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The System Jobs window, where it is used in the procedure to update the Finest Partition for the Pavement Structure Profile. (It updates the information needed for the Pavement Structure [Profile/Cross Section] window.)</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click in the window, the following special command is displayed in a shortcut menu:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Fill Table</text><text styleclass="Bullet L1 Last" translate="true"> — This command causes the &quot;lanes&quot; information for the entire road network to be re-generated from the Pavement Management window&apos;s road sections and number of lanes values. Each pavement management record yields either one or two &quot;lanes&quot; records. Two records are generated, one for each direction, whenever the pavement management record&apos;s direction is &quot;All.&quot; &#160;In this case, the number of lanes is also halved.</text></li>
    </list>
  </body>
</topic>
