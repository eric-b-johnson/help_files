﻿<?xml version="1.0" encoding="UTF-8"?>
<topic template="Default" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Projects Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Projects Tab</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Projects tab in the middle pane shows the projects for the structure selected in the Structure Network Analysis pane at the top of the window. Projects shown in this tab may be created, edited, or deleted.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Projects are divided into four types:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The first three types (BMS Type 1, BMS Type 2, and BMS Type 3) are for various combinations of A, B, and 0 structure groups. (The first column shows these different types.) These three types are automatically calculated using one of the following commands found on the right-click shortcut menu:</text></li>
    </list>
    <list id="4" type="ul" listtype="bullet" formatstring="o" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L2"><text styleclass="Command" translate="true">Compose All Default Projects</text><text styleclass="Bullet L2" translate="true"> -- This command creates projects for all three types.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Command" translate="true">Compose Default Project 1</text><text styleclass="Bullet L2" translate="true"> -- This command creates projects for BMS Type 1.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Command" translate="true">Compose Default Project 2</text><text styleclass="Bullet L2" translate="true"> -- This command creates projects for BMS Type 2.</text></li>
      <li styleclass="Bullet L2 Last"><text styleclass="Command" translate="true">Compose Default Project 3</text><text styleclass="Bullet L2 Last" translate="true"> -- This command creates projects for BMS Type 3.</text></li>
    </list>
    <list id="5" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The fourth type is called Proj Inspector 1. It is used for projects entered directly into this tab with the right-click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1 Last" translate="true"> command.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: All manually created projects (that is, those created by the user in this window) should be the Proj Inspector 1 type. Otherwise, the project&apos;s information will be lost when any of the </text><text styleclass="Command" translate="true">Compose Default Project</text><text styleclass="Body Text" translate="true"> commands are executed.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">To update cost and benefit information for the current project, use the </text><text styleclass="Command" translate="true">Update Cost/Benefits</text><text styleclass="Body Text" translate="true"> command that is found by right-clicking the project&apos;s record. This command updates the cost and benefit information based upon the treatment information for the project and the structure’s attributes.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The </text><text styleclass="Text As Shown on Screen" translate="true">Project Cost</text><text styleclass="Body Text" translate="true"> column in this tab is the sum of the treatment costs from the Treatments pane at the bottom of the window. The treatment costs in the Treatments pane are calculated as unit cost times the cost expression (both set in the Treatment Assignments window).</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Project Benefits are set in the </text><text styleclass="Text As Shown on Screen" translate="true">CPI Av</text><text styleclass="Body Text" translate="true"> [Average] and </text><text styleclass="Text As Shown on Screen" translate="true">CPI Crit</text><text styleclass="Body Text" translate="true"> [Critical] condition indexes columns. They are calculated according to the formulae shown in the BMS Module document.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Finally, a project may be copied to a work plan by utilizing the right-click </text><text styleclass="Command" translate="true">Copy Project to MWP</text><text styleclass="Body Text" translate="true"> command. This command displays a new window in which you may select the work plan and year as well as assign the status for the project in the designated work plan.</text></para>
  </body>
</topic>
