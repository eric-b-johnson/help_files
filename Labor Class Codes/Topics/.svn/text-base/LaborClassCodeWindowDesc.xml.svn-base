﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Description of the Labor Class Codes Window</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Description of the Labor Class Codes Window</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Labor Class Code window contains two panes: &#160;on the left, a tree view that shows the hierarchical arrangement of the codes; and on the right a table view that provides additional information about each code. The tree view is expanded by clicking a plus sign (+) or contracted by clicking a minus sign (–).</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;The value in the </text><text styleclass="Text As Shown on Screen" translate="true">Max Hours Allowed</text><text styleclass="Body Text" translate="true"> column sets the threshold for excessive hours. (If the field is blank, then the default value is 12 hours.) When the value in this column is exceeded during time entry (for example, in the Daily Log window), the system asks the user whether the entered time is correct.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">When you select a code in the tree view pane, the row corresponding to that code is highlighted and displayed in the table on the right. The converse is also true — selecting a row also selects a code in the tree view.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a row in the table view pane, the system displays a shortcut menu. This menu contains the following command in addition to the common commands:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Add Branch</text><text styleclass="Bullet L1 Last" translate="true"> — This command adds a new, subordinate node to the tree view and a corresponding row in the table view. </text><link displaytype="text" defaultstyle="true" type="topiclink" href="HowtoInsertaNewLowerLev" styleclass="Bullet L1 Last" translate="true">Click here</link><text styleclass="Bullet L1 Last" translate="true"> for the procedure on how to add a new labor class code.</text></li>
    </list>
  </body>
</topic>
