﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Setup Analysis Defects</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Setup Analysis Defects</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Setup Defects window defines the defects utilized by your agency and how a defect score is calculated. Defect scores are important for maintenance analysis.</text></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><image src="hmtoggle_plus0.gif" scale="100.00%" styleclass="Internal Heading"></image><tab /><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="false" translate="true" src-collapsed="hmtoggle_plus0.gif" src-expanded="hmtoggle_plus1.gif" styleclass="Internal Heading"><caption translate="true"><![CDATA[Description of the Columns]]></caption></toggle></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">This window provides the following columns:</text></para>
          <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Asset Type — This column shows the asset type to which the feature belongs.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Best Value — This column stores the defect value that indicates perfect condition.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Expression for Inv. Totals — This column provides the SQL script that determines how the total amount of the asset for the defect is calculated.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Feature — This column lists the defect elements.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Groovy Script ID — This column contains a drop-down list of Groovy scripts. You select the appropriate Groovy script to determine the “Scenario Cost” that will achieve a “Desired LOS,” where the “Current Cost” and the “Current LOS” are known.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">MMS Util Function — The MMS Utility function is the defect score/grade-related function that is used for optimization in maintenance analysis. For example, if you want to optimize directly on defect score, then the utility function is simply the defect score. If you want to give 20% extra priority to interstates, then the utility function would multiply defect scores by 1.2 for interstates.</text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This column contains a drop-down list of Groovy scripts. You select the appropriate Groovy script that defines and sets the utility function.</text></para>
          <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">SQL for Defect Summary — This column provides the SQL script that determines the defect score for the feature.</text></li>
            <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Worst Value — This column stores the defect value that indicates the lowest possible condition.</text></li>
          </list>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
