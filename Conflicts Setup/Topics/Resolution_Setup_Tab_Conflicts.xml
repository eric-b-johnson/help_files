﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Description of the Resolution Setup Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Description of the Resolution Setup Tab</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Resolution Setup tab contains two panes:</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The Objects Tree pane on the left lists the tables that may be downloaded to computing devices used in the field. A check mark in the square beside the table name indicates that conflict resolution is enabled for the table — unless the </text><text styleclass="Text As Shown on Screen" translate="true">All</text><text styleclass="Bullet L1" translate="true"> parent node is checked, in which case no check boxes are selected for individual tables, but all have conflict resolution enabled.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;When </text><text styleclass="Bullet L1" translate="true">the </text><text styleclass="Text As Shown on Screen" translate="true">All</text><text styleclass="Bullet L1" translate="true"> parent node is checked, all listed tables have the same conflict resolution types. You may assign an individual table to have a different resolution type from the </text><text styleclass="Text As Shown on Screen" translate="true">All</text><text styleclass="Bullet L1" translate="true"> setting by clicking the check box next to the table name and then selecting the desired resolution type(s) in the right pane.</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The Objects List pane on the right shows the same tables in a table format and provides three columns to assign how to resolve conflicts for the action to be taken (insert, update, or delete). When you select a column in the left pane, the system also selects the corresponding record in the table in the right pane. Also, if the </text><text styleclass="Text As Shown on Screen" translate="true">All</text><text styleclass="Bullet L1" translate="true"> parent node is checked, selecting a value for one table also selects that value for all tables that are not individually checked.</text></li>
    </list>
    <para styleclass="L1 Indent" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:5px; margin-left:48px; line-height:1.0; white-space:normal; page-break-inside:auto; page-break-after:auto; tabstops:none;"><text styleclass="L1 Indent" translate="true">The types of conflict resolutions that may be configured are described below:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#111;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">Server-priority — The system automatically assigns the table in the main database as having precedence.</text></li>
      <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">Client-priority — The system automatically assigns the table arriving from the field (the check-in table) as having precedence.</text></li>
      <li styleclass="Bullet L2 Last"><text styleclass="Bullet L2 Last" translate="true">Manual — Human intervention is required to resolve the conflict in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Manage_Transactions" domain="..\Manage Transactions\Manage Transactions.hmxp" styleclass="Bullet L2 Last" translate="true">Manage Offline Transactions</link><text styleclass="Bullet L2 Last" translate="true"> window.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">The following table summarizes the result of conflict resolution depending on the resolution type. The </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Approval_Setup_Tab_Conflicts" styleclass="L1 Indent" translate="true">type of approval</link><text styleclass="L1 Indent" translate="true"> enabled for a table may further affect the final resolution (for example, if the type of approval for a table is set to </text><text styleclass="Text As Shown on Screen" translate="true">Any Conflict</text><text styleclass="L1 Indent" translate="true">, then regardless of the operation to be performed or the type of conflict resolution, the conflict must be resolved manually).</text></para>
    <para styleclass="L1 Indent"><table styleclass="Default" rowcount="10" colcount="3" style="cell-padding:4px; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid;">
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Operation to be Performed</text></para>
        </td>
        <td style="width:165px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Resolution Type</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Resolution Result</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td rowspan="3" style="vertical-align:top; width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Insert</text></para>
        </td>
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Server-priority</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Discard INSERT.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Client-priority</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Change INSERT to UPDATE.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Manual</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Resolve manually.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td rowspan="3" style="vertical-align:top; width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Delete</text></para>
        </td>
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Server-priority</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Discard DELETE.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Client-priority</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Apply DELETE.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Manual</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Resolve manually.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td rowspan="3" style="vertical-align:top; width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Update</text></para>
        </td>
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Server-priority</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Discard UPDATE.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Client-priority</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:5px; margin-left:0px; line-height:1.0; white-space:normal; page-break-inside:auto; page-break-after:auto; tabstops:none;"><text styleclass="Table Body Text" translate="true">If a record in the check-in table is different than the record in the main database, apply UPDATE.</text></para>
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">If a record in the check-in table does not exist in the main database, change UPDATE to INSERT.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:165px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Manual</text></para>
        </td>
        <td style="width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Resolve manually.</text></para>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
