﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Import</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Import</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Import feature allows you to:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Import data from fixed-length ASCII text files, comma-separated (CSV) ASCII text files, Oracle database tables, or Microsoft Excel files into the application.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Configure import processes.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;The Tables window also provides an import feature to import data directly into a table. </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Import_Config_Wndo_Desc" domain="..\Tables\Tables.hmxp" styleclass="Body Text" translate="true">Click here</link><text styleclass="Body Text" translate="true"> for more information.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Importing data is a three-step process involving three files as shown in the figure below. The Load Batch and Process Batch processes copy data from one file to the next. The View Batch process identifies which records have error messages (which means that these records were not moved to the target table).</text></para>
    <para styleclass="Normal"><text styleclass="Normal" style="font-size:5pt;" translate="true">&#32;</text></para>
    <para styleclass="Body Text"><image src="Import Process.gif" scale="100.00%" style="font-family:&apos;MS Sans Serif&apos;; font-size:8pt; color:#000000;"></image></para>
    <para styleclass="Normal"></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;Importing normally occurs on an on-going basis. The data that you are importing is called a &quot;batch.&quot; A source file is a batch. Also, data is grouped by batch in the intermediate Oracle table. Once data is processed into the target Oracle table, it loses its batch designation.</text></para>
  </body>
</topic>
