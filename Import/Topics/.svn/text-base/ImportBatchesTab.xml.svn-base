﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Import&apos;s Batches Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Import&apos;s Batches Tab</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">For the import process selected in the All Imports tab, the Import&apos;s Batches tab is used to initiate the import data process and also contains a log of all import attempts (batches). (A batch is a set of data that is imported into the system.)</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The import process generally occurs as follows:</text></para>
    <list id="4" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The desired import process is selected in the All Imports tab.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Import&apos;s Batches tab, the batch starts as an input file.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The user right-clicks the tab to display the shortcut menu and then clicks </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true"> to copy the batch into the intermediate Oracle table.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The user right-clicks </text><text styleclass="Bullet L1" translate="true">the tab to display the shortcut menu</text><text styleclass="Bullet L1 Last" translate="true"> and clicks </text><text styleclass="Command" translate="true">Process Batch</text><text styleclass="Bullet L1 Last" translate="true"> to move all valid data records into the system from the intermediate Oracle table, with invalid records remaining in the intermediate Oracle file for review.</text></li>
    </list>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><image src="hmtoggle_plus0.gif" scale="100.00%" styleclass="Internal Heading"></image><tab /><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="false" translate="true" src-collapsed="hmtoggle_plus0.gif" src-expanded="hmtoggle_plus1.gif" styleclass="Internal Heading"><caption translate="true"><![CDATA[Description of the Columns in the Import's Batches Tab]]></caption></toggle></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Import&apos;s Batches tab contains the following columns:</text></para>
          <list id="5" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Args — This column shows the Oracle statements, if any, that are used in processing the batch.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Import Batch ID — This column shows the identification number for the batch.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Rows in Batch — This column identifies how many records remain in the batch (that is, in the intermediate Oracle table). Before the </text><text styleclass="Command" translate="true">Process Batch</text><text styleclass="Bullet L1" translate="true"> command is run, this number is the total number of records in the input file. After the </text><text styleclass="Command" translate="true">Process Batch</text><text styleclass="Bullet L1" translate="true"> command is run, this number is the number of invalid records that remain (the valid records having been moved into the main database).</text></li>
            <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Source Name — This column shows the input file name for the batch including its subdirectories.</text></li>
          </list>
        </td>
      </tr>
    </table></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><image src="hmtoggle_plus0.gif" scale="100.00%" styleclass="Internal Heading"></image><tab /><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="false" translate="true" src-collapsed="hmtoggle_plus0.gif" src-expanded="hmtoggle_plus1.gif" styleclass="Internal Heading"><caption translate="true"><![CDATA[Description of the Right-click Shortcut Menu Commands]]></caption></toggle></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a record in this tab, the system displays a shortcut menu. This menu shows the common commands along with the following special commands:</text></para>
          <list id="6" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true"> — This command loads data from the user-selected input file into the batch (the intermediate Oracle table). If this load is successful, a batch record is added to the table. For further information, see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="HowtoImportData_ImportWndo" styleclass="Bullet L1" translate="true">How to Import</link><text styleclass="Bullet L1" translate="true">.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Process Batch</text><text styleclass="Bullet L1" translate="true"> — This command moves all error-free records from the intermediate Oracle table to the application&apos;s target table. This command processes the batch by:</text></li>
          </list>
          <list id="7" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">Validating all records in the batch and attaching error messages to the invalid records.</text></li>
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">Moving all valid records into the application.</text></li>
            <li styleclass="Bullet L2 Last"><text styleclass="Bullet L2 Last" translate="true">Removing the valid records from the batch (the intermediate Oracle table).</text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" style="font-size:5pt;" translate="true">&#32;</text><text styleclass="L1 Indent" translate="true">For further information, see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="HowtoImportData_ImportWndo" styleclass="L1 Indent" translate="true">How to Import</link><text styleclass="L1 Indent" translate="true">.</text></para>
          <list id="6" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Reimport Batch</text><text styleclass="Bullet L1" translate="true"> — This command performs the same actions as the </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true"> command except that the result that goes into the intermediate Oracle table overwrites the currently selected batch.</text></li>
            <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">View Batch</text><text styleclass="Bullet L1 Last" translate="true"> — This command allows you to see the data in the batch as well as the associated error messages for invalid records.</text></li>
          </list>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
