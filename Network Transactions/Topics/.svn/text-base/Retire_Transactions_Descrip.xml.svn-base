﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Descriptions of &quot;Retire&quot; Network Transactions</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Descriptions of &quot;Retire&quot; Network Transactions</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The following table describes the function of each retire network transaction. (&quot;Retire&quot; network transactions are those transactions that delete event data.) For information on what inputs are required by each transaction, </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Inputs_for_Network_Transactions" styleclass="Body Text" translate="true">click here</link><text styleclass="Body Text" translate="true">.</text></para>
    <para styleclass="Body Text"><table rowcount="8" colcount="2" style="width:auto; cell-padding:3px; cell-spacing:0px; page-break-inside:avoid; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid; background-color:none; head-row-background-color:none; alt-row-background-color:none;">
      <tr style="vertical-align:top">
        <td style="width:250px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Transaction Type</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Description</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Create a gap in the route.</text></para>
        </td>
        <td style="vertical-align:top; width:400px;">
          <para styleclass="Table Body Text" style="text-align:right;"><text styleclass="Table Body Text" translate="true">This transaction creates a gap in a route. The length of the route is not affected. (Once the gap is created, the system stores the details in the Network Gaps table.)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:250px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Re-align end with retirement.</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This transaction creates a new road section at the end of an existing route and deletes the old section from the network.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:250px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Re-align middle with retirement.</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This transaction creates a new road section within an existing route and deletes the old section from the network.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:250px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Re-align start with retirement.</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This transaction creates a new road section at the beginning of an existing route and deletes the old section from the network.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Retire a route.</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This transaction removes a route from the network and deletes all data associated with the route.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Retire the end of a route.</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This transaction removes a portion of a route from the end of the route. Any data associated with the removed portion is also deleted from the system.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:250px;">
          <para styleclass="Table Body Text" style="text-align:left;"><text styleclass="Table Body Text" translate="true">Retire the start of a route.</text></para>
        </td>
        <td style="width:400px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This transaction removes a portion of a route from the beginning of the route. Any data associated with the removed portion is also deleted from the system. (The measurement values of the route are not changed.)</text></para>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
