﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">BMS Network Master</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">BMS Network Master</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Network Master window is the starting point for all network analyses (short-term as well as long-term). These analyses depend on the columns included in the Network Master File (NMF). The window contains the most current inventory and inspection information for the structures (bridge and culverts) assigned to your administrative unit. If desired, you may filter this window to show just those structures in which you are interested.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The NMF is the input data stream for network analysis. It must be re-calculated at least once per year for the current data set to be used in network analysis. It should also be re-calculated each time any of the condition indicator settings change.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click this window, the common commands are displayed along with the following special commands that are used for the NCDOT implementation:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Publish Record Card</text><text styleclass="Bullet L1" translate="true"> — This command displays the bridge ID card per NCDOT&apos;s specifications.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">See Bridge In WIGINS</text><text styleclass="Bullet L1 Last" translate="true"> — This command opens the NCDOT WIGINS database and allows you to review the information stored in this database for the selected structure.</text></li>
    </list>
  </body>
</topic>
