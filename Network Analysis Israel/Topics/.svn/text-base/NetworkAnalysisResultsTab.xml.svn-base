﻿<?xml version="1.0" encoding="UTF-8"?>
<topic template="Default" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Results Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Results Tab</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Results tab provides a list of pavement sections that the network analysis scenario produced, based upon the input criteria and network analysis type chosen. These pavement sections constitute the recommended work plan. While this tab is always displayed and may contain information, the information is only relevant once a scenario has been run.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The tab provides the Work Plan pane and four constraint panes (Budget, Index, RSL [Remaining Service Life], and % Above Thr[eshold]).</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Work Plan pane shows:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The year work should be performed based upon the analysis.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The pavement sections to be improved.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The new road structure category after the treatment is applied.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The treatment applied to the pavement section.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The project price (which is a function of the unit cost and pavement section dimensions).</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The priority as defined by the method type.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The project status code.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The budget group of the applied treatment.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The constraint panes at the bottom of the tab show the constraint target selected in the Setup tab as well as the actual value achieved during analysis. For the constraint selected as part of the analysis method, the results should very closely match the constraint target value, since, by definition, the analysis stops when this constraint is reached. Optionally, if information is entered in the other constraint tabs of the Setup tab, then the corresponding pane in the Results tab will show the target value entered in the Setup tab and the actual value achieved as a result of the analysis.</text></para>
  </body>
</topic>
