﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Cost Factors Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Cost Factors Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Cost Factors pane shows the costs factors for various years to be applied to the activities listed in the left pane. The system uses the entered cost factors to calculate average weighted unit costs (and employee hours) for every activity that has historic work order information during the selected year.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Note: &#160;Each year you should update the cost factor as follows:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Cost Factor pane, insert or edit a record for each year you want to contribute to the activity cost aggregation. </text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Cost Factor</text><text styleclass="Bullet L1" translate="true"> column, enter a number to represent the weighted contribution to the cost factor calculation. </text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click the </text><image src="Icon_Save.png" scale="90.00%" width="19" height="19" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1" translate="true"> icon. </text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Weighted Average Costs by Activity pane, right-click and then click </text><text styleclass="Command" translate="true">Refresh Cost</text><text styleclass="Bullet L1" translate="true"> to update the average historical unit costs (and employee hours) for all activities with historic data. </text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click the </text><image src="Icon_Save.png" scale="90.00%" width="19" height="19" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1" translate="true"> icon.</text></li>
    </list>
  </body>
</topic>
