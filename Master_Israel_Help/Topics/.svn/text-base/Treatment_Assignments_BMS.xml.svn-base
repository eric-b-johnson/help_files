﻿<?xml version="1.0" encoding="UTF-8"?>
<topic template="Default" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Treatment Assignments Window</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Treatment Assignments Window</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">In the Treatment Assignments window three different treatment assignment strategies (treatment levels) are available. &#160;For each level, a treatment can be assigned for any combination of:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Element.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Defect.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Severity.</text></li>
    </list>
    <list id="4" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Extent</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Once a treatment is assigned, then for this particular combination of level, element, defect, severity, and extent you also &#160;enter attributes in the following columns:</text></para>
    <list id="5" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Cost Formula -- Three cost formula columns are provided. Each uses some combination of unit cost, dimensions (as defined by the </text><text styleclass="Text As Shown on Screen" translate="true">Measurement Units</text><text styleclass="Bullet L1" translate="true"> column), and extent --- all from the same record.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Unit Cost -- This column provides the unit cost used in the cost formula.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Measurement Unit -- This column identifies which measurement from the bridge inventory data is used in the cost formula as the “dimensions.”</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">BMS 1st Treatment -- This column provides a check box. When the check box is selected, the treatment is used in the treatment assignment for the recommended projects.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Improved BCI -- This column shows the element condition index after treatment.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Delta BCI -- This column shows the improvement of the element condition index after the treatment is applied.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">New Severity -- This column shows the new severity after the treatment is applied.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">New Extent -- This column shows the new extent after the treatment is applied.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Rehab -- When the check box in this column is selected, the treatment is a rehabilitation treatment.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">PM -- When the check box in this column is selected, the treatment is a preventive maintenance treatment.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Maint. -- When the check box in this column is selected, the treatment is a maintenance treatment.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;The last three columns are used to compose the recommended projects, depending on the structure’s group.</text></para>
    <para styleclass="Body Text"></para>
  </body>
</topic>
