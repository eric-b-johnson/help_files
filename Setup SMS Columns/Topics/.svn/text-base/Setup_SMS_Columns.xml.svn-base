﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Setup SMS Columns</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Setup SMS Columns</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">You use the Setup MMS Columns window to configure what columns will be used in safety analysis.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;If you insert a new record into this window, you must enter a column ID and this column ID must exist in the Columns window. Furthermore, its column type must either be numeric or list. If these conditions are not met, an error message will be displayed. </text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The following columns are available in this window:</text></para>
    <list id="282" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Column ID — This column shows the internal identification number of the column.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Column Label — This column shows the name of the column as it appears in the AgileAssets system.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Is Crash Scope Var — When this check box is selected, the column may be used as a filter variable in the Crash Filter window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Is Network Scope Var — When this check box is selected, the column may be used as an additional variable for filtering with the SQL main profile. If selected, the Update SQL Main Profile must be completed with the necessary SQL query.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Present in Blackspot Table — When this field is set to 1, the column will appear in the blackspot table.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Threshold Column Role — This field sets whether the column is a variable for length, performance measure, or functional class.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Update Order — When data is updated, the value in this column determines when the column is updated in regard to other columns being updated.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Update SQL Blackspot — This field contains the SQL query to update Hotspot variables in the Hotspots window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Update SQL Main Profile — This field contains the SQL query to update Main Profile variables.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Update SQL Network Scope &#160;— This field contains the SQL query to update network scope variables.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Update SQL Secondary — This field contains the SQL query to update Secondary Profile variables.</text></li>
    </list>
    <list id="283" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1 Last" translate="true">Use Crash Filter — </text><text styleclass="Bullet L1" translate="true">When this check box is selected, the column may be used for filtering crashes</text><text styleclass="Bullet L1 Last" translate="true">.</text></li>
    </list>
    <para styleclass="L1 Indent" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:5px; margin-left:48px; line-height:1.0; white-space:normal; page-break-inside:auto; page-break-after:auto; tabstops:none;"><text styleclass="L1 Indent" translate="true">Note: &#160;For a column to be considered as a crash filter, the column ID of the column in this window must also be the column ID in the SMS_CRASH_DATA table.</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;Do not select this check box for the </text><text styleclass="Text As Shown on Screen" translate="true">Crash Rate by VMT</text><text styleclass="L1 Indent" translate="true"> and </text><text styleclass="Text As Shown on Screen" translate="true">Crash Rate by Length</text><text styleclass="L1 Indent" translate="true"> columns.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click this pane, the following special commands are available along with the common commands:</text></para>
    <list id="284" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Add Existing Column</text><text styleclass="Bullet L1" translate="true"> — This command displays a dialog box so you may add an existing column to the window.</text></li>
    </list>
  </body>
</topic>
