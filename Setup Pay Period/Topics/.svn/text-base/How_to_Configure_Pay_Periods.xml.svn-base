﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Generate Pay Periods for a New Year</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Generate Pay Periods for a New Year</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Typically, before the start of a new year you need to generate the set of pay periods for the coming year. This is accomplished by following these steps:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Open the Custom Calendars window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Select the year in the drop-down field at the top of the window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the left pane, select the </text><text styleclass="Text As Shown on Screen" translate="true">Regular</text><text styleclass="Bullet L1" translate="true"> calendar. The right pane shows the days of the year with Saturdays and Sundays set to zero hours. Holidays and other non-working days have not yet been configured.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the right pane, find each non-working day other than weekends and change the value in the </text><text styleclass="Text As Shown on Screen" translate="true">Eff Hours</text><text styleclass="Bullet L1" translate="true"> column to zero for the non-working day. (If any weekend day is a work day, change the zero value to 8 for this day.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">When finished, click </text><image src="Icon_Save.png" scale="90.00%" width="19" height="19" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1" translate="true">.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Open the Pay Periods window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Right-click the record showing the pay period type being used for pay periods and then click </text><text styleclass="Command" translate="true">Initial Fill for a Year</text><text styleclass="Bullet L1" translate="true">. The system responds by asking for the year. (The pay period type being used for pay periods is shown in the Employee Pay Period Type window.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the dialog box, click the down arrow and then click the year for which pay periods are being generated.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;If the year is not shown in the drop-down list, add the year to the SETUP_YEAR table in the database.</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click </text><text styleclass="Command" translate="true">OK</text><text styleclass="Bullet L1" translate="true">. The system fills the right pane with the pay periods for the selected year.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click </text><image src="Icon_Save.png" scale="90.00%" width="19" height="19" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1" translate="true">.</text></li>
    </list>
    <para styleclass="Body Text"></para>
  </body>
</topic>
