﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Create the View for GIS_DATA_SOURCE</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Create the View for GIS_DATA_SOURCE</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The GIS_DATA_SOURCE field in the Theme Properties dialog box shows the asset&apos;s inventory table name, prefaced with </text><text styleclass="Text As Shown on Screen" translate="true">GIS_</text><text styleclass="Body Text Before List" translate="true">. To configure the view for this, perform the following steps:</text></para>
    <list id="2" type="ol" listtype="decimal" formatstring="%0:s." format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:'Book Antiqua'; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Open the Views window, which is typically found in the Utilities menu of the System module.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Right-click in the window and then click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">. The system displays a dialog box asking for the name of the view.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the dialog box, enter the name you used in the GIS_DATA_SOURCE field. The system adds a new record to the window. Note that the </text><text styleclass="Text As Shown on Screen" translate="true">In DB?</text><text styleclass="Bullet L1" translate="true"> and </text><text styleclass="Text As Shown on Screen" translate="true">Is Valid?</text><text styleclass="Bullet L1" translate="true"> check boxes are not checked. This is because the view has no data — and so data will need to be added to the view.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Ensure that the new record is selected and then click the Source tab. The system displays the Source tab with the message</text><text styleclass="Text As Shown on Screen" translate="true"> CREATE OR REPLACE VIEW (view name) AS</text><text styleclass="Bullet L1" translate="true">. </text><text styleclass="L1 Indent" translate="true">If you compare this to another GIS_(Inventory Table), it looks similar to the following:</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">CREATE OR REPLACE VIEW GIS_SNOW_ROUTE_INVENTORY</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">(SNOW_ROUTE_ID,PERIODIC_MAINT_ID,SNOW_ROUTE_NAME,SNOW_ROUTE_CLASS_CODE_ID,SNOW_ROUTE_STATUS_ID,COMMENT_STR,COMMENT_ID,USER_UPDATE,DATE_UPDATE,OWNER_ID,DISTRICT_ID,CREW_ID,OWNER_ASSIGNED,SNOW_ROUTE_DESCR,LOC_IDENT,WY_COUNTY_ID,DEADHEAD_MILES,SNOW_LANE_MILES,OFFSET_FROM,ROUTE_ID,OFFSET_TO,LANE_DIR,LANE_ID,PERPEN_OFFSET)</text></para>
    <para styleclass="L1 Indent"><text styleclass="Bullet L1" translate="true">AS</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">SELECT a.SNOW_ROUTE_ID,a.PERIODIC_MAINT_ID,a.SNOW_ROUTE_NAME,a.SNOW_ROUTE_CLASS_CODE_ID,a.SNOW_ROUTE_STATUS_ID,a.COMMENT_STR,a.COMMENT_ID,a.USER_UPDATE,a.DATE_UPDATE,a.OWNER_ID,a.DISTRICT_ID,a.CREW_ID,a.OWNER_ASSIGNED,a.SNOW_ROUTE_DESCR,a.LOC_IDENT,a.WY_COUNTY_ID,a.DEADHEAD_MILES,a.SNOW_LANE_MILES,c.OFFSET_FROM,c.ROUTE_ID,c.OFFSET_TO,c.LANE_DIR,c.LANE_ID,c.PERPEN_OFFSET</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">FROM SNOW_ROUTE_INVENTORY a, SETUP_LOC_IDENT c </text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">WHERE a.LOC_IDENT = c.LOC_IDENT</text><tab/></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This SQL is creating a View with all the columns from SNOW_ROUTE_INVENTORY and adding the columns OFFSET_FROM, ROUTE_ID, OFFSET_TO, LANE_DIR, LANE_ID, PERPEN_OFFSET from SETUP_LOC_IDENT.  You may thus use this as a template to set up your new view by replacing *inventory table* and *column names*:</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">CREATE OR REPLACE VIEW GIS_*inventory table*</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">(*column names*, OFFSET_FROM, ROUTE_ID, OFFSET_TO, LANE_DIR, LANE_ID, PERPEN_OFFSET)</text></para>
    <para styleclass="L1 Indent"><text styleclass="Bullet L1" translate="true">AS</text><tab/></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">SELECT</text><tab/><tab/></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">a.*column names*, c.OFFSET_FROM, c.ROUTE_ID, c.OFFSET_TO, c.LANE_DIR, c.LANE_ID, c.PERPEN_OFFSET</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">FROM *inventory table* a, SETUP_LOC_IDENT c </text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">WHERE a.LOC_IDENT = c.LOC_IDENT</text></para>
    <list id="3" type="ol" listtype="decimal" formatstring="%0:s." format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="5" styleclass="Bullet L1" style="font-family:'Book Antiqua'; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Once you have completed the Source tab, right-click and then click </text><text styleclass="Command" translate="true">Apply Changes</text><text styleclass="Bullet L1" translate="true">.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click the Views tab. The record now shows check marks in the </text><text styleclass="Text As Shown on Screen" translate="true">In DB?</text><text styleclass="Bullet L1" translate="true"> and </text><text styleclass="Text As Shown on Screen" translate="true">Is Valid?</text><text styleclass="Bullet L1" translate="true"> check boxes. This means data is now in the view and hence  will be available for display on a map via the new theme.</text></li>
    </list>
    <para styleclass="Bullet L1"></para>
  </body>
</topic>
