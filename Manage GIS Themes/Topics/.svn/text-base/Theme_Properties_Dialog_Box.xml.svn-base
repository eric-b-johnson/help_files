﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Theme Properties Dialog Box</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Theme Properties Dialog Box</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">You display the Theme Properties dialog box by right-clicking a theme name and then clicking the </text><text styleclass="Command" translate="true">Edit Themes</text><text styleclass="Body Text" translate="true"> command. This dialog box contains fields for configuring the theme. How you configure these fields depends on the type of GIS data source (as selected in the </text><text styleclass="Text As Shown on Screen" translate="true">GIS Source</text><text styleclass="Body Text" translate="true"> field, the contents of which are configured in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Config_GIS_Data_Sources" domain="..\Configure GIS Data Sources\Configure GIS Data Sources.hmxp" styleclass="Body Text" translate="true">Configure GIS Data Sources</link><text styleclass="Body Text" translate="true"> window).</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The following table shows the allowed types of GIS data sources and what is placed in each field of the Theme Properties dialog box.</text></para>
    <para styleclass="Normal"><table rowcount="16" colcount="8" style="width:auto; cell-padding:4px; cell-spacing:0px; page-break-inside:auto; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid; background-color:none; head-row-background-color:none; alt-row-background-color:none;">
      <thead style="vertical-align:top">
        <td rowspan="2" style="vertical-align:bottom; width:77px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Theme Properties Field</text></para>
        </td>
        <td colspan="7" style="vertical-align:bottom; width:494px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">GIS Data Source Type</text></para>
        </td>
      </thead>
      <thead style="vertical-align:top">
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">ArcIMS</text></para>
        </td>
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">ArcSDE</text></para>
        </td>
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Image</text></para>
        </td>
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Location</text></para>
        </td>
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Shape</text></para>
        </td>
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Spatial</text></para>
        </td>
        <td style="vertical-align:bottom; width:50px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">XY</text></para>
        </td>
      </thead>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Theme Name</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This field shows the name of the theme as displayed in the Map pane.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">GIS Theme Type</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This field is a drop-down list that indicates the type of GIS theme (point, line, polygon, or image).</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Theme Size</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This field provides the width or diameter of lines or points (in pixels) that appear on the map.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Color</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This field shows the color assigned to the theme, which is displayed on the map when the theme is selected for display. If you double-click the color, the system displays the color palette dialog box. You may then select a different color for the theme (by either typing a color ID into the field at the top of the color palette dialog box or by clicking a color in the palette) and then clicking the OK button.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">GIS Data Source</text></para>
        </td>
        <td style="vertical-align:top; width:87px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Path to desired layer in a semi-colon separated list (no ending semi-colon).</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Table name of desired layer.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Name of the Image file.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Table name of desired layer.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Name of the Shape file.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Table name of desired layer.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Table name of desired layer.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">GIS Data Source Shp Col</text></para>
        </td>
        <td style="vertical-align:top; width:87px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Same as Shape.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td colspan="4" style="vertical-align:top; width:275px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">ID Column in GIS Data source that identifies each feature to which data will be linked. If this column is null, features are given a dummy sequence by their record number.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">GIS Data Source Lbl Col</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The column in the GIS source data that contains the label for each unique feature.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">GIS Data Source Dir Col</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">No longer in use.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Where Clause</text></para>
        </td>
        <td style="vertical-align:top; width:87px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">SQL statement that limits the data that is retrieved.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">SQL statement that limits the data that is retrieved.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Ref Data Source</text></para>
        </td>
        <td style="vertical-align:top; width:87px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Same as Shape.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td colspan="4" style="vertical-align:top; width:275px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The table within the AgileAssets schema that is a crosswalk between the GIS_DATA_SOURCE_SHP_COL and REF_DATA_SOURCE_DAT_COL columns.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Ref Data Source Dat Col</text></para>
        </td>
        <td style="vertical-align:top; width:87px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Same as Shape.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td colspan="4" style="vertical-align:top; width:275px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">A column within the REF_DATA_SOURCE table that matches data from within the AgileAssets schema.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Ref Data Source Shp Col</text></para>
        </td>
        <td style="vertical-align:top; width:87px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td style="vertical-align:top; width:59px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Same as Shape.</text></para>
        </td>
        <td style="vertical-align:top; width:58px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Leave blank.</text></para>
        </td>
        <td colspan="4" style="vertical-align:top; width:275px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The column within the REF_DATA_SOURCE table that matches the value in the GIS_DATA_SOURCE_SHP_COL column.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">CRS Description</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">This should be null for most layers if the coordinate reference system (CRS) is included in the data source. For image files, the system will look for a *.prj file to use.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="vertical-align:top; width:77px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Window ID</text></para>
        </td>
        <td colspan="7" style="vertical-align:top; width:494px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The Window ID of the popup window that displays data from a map that shows asset inventory.</text></para>
        </td>
      </tr>
    </table></para>
    <para style="margin-bottom:8px;"></para>
  </body>
</topic>
