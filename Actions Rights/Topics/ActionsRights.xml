﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Actions</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Actions</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; font-weight:normal; font-style:normal; text-transform:none; vertical-align:baseline; color:#ff0000; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">Note: &#160;Only personnel authorized as a System Administrator should be allowed read/write access to this window.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">This window is used to re-name and set access levels for user action properties (that is, the commands in the shortcut menus found by right-clicking a pane or window [these are termed &quot;right-click commands&quot;]). It is also used to require that a user enter his or her password to execute certain commands.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Note that three windows maintain access level settings, each of which contributes to what data may be viewed and maintained by an access profile. These are:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Access Level Settings — This window is where access profiles are created and maintained.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Actions — This is the window described in this topic, which is where you specify a user&apos;s ability to view and execute right-click commands.</text></li>
    </list>
    <list id="4" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Columns — This is where the user&apos;s ability to view and/or edit particular columns in a window is specified.</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Within a window, the user&apos;s access to right-click commands and columns depends on how his or her access level settings relate to the window access level setting in the user&apos;s access profile:</text></para>
    <list id="5" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">When the window access level setting is greater than or equal to the access level setting for the right-click command, then the user can activate the command in the window. Otherwise, the command is disabled for that user.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">When the window access level setting is greater than or equal to the access level setting in the </text><text styleclass="Text As Shown on Screen" translate="true">Right to See</text><text styleclass="Bullet L1" translate="true"> column, then the user can see the column in the window. Otherwise, the user cannot see the column.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">When the window access level setting is greater than the access level setting in the </text><text styleclass="Text As Shown on Screen" translate="true">Right to Edit</text><text styleclass="Bullet L1 Last" translate="true"> column, then (assuming that the user has edit capability in the window in general) the user can edit the column. Otherwise, the user cannot edit the column.</text></li>
    </list>
  </body>
</topic>
