﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Setup Activity Shortlist</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Setup Activity Shortlist</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">This window allows you to create a list of &quot;overhead&quot; work activities for your administrative unit so that overhead hours may be entered into the Timesheet window. (Overhead activities are essentially general activities rather than specific, readily-identifiable work tasks.) The records in this window appear in the drop-down list of the dialog box that appears when inserting records into the Timesheet window.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">You add overhead work activities to the list shown in the Setup Activity Shortlist window by right-clicking in the window to display a shortcut menu and then clicking </text><text styleclass="Command" translate="true">Edit Selection</text><text styleclass="Body Text" translate="true">. This command displays a dialog box that shows all activities assigned to the Employees asset type in the Setup Asset Activity window. To select an activity, right-click the activity and then click one of the selection commands. When you have made all of your selections, click </text><text styleclass="Command" translate="true">OK</text><text styleclass="Body Text" translate="true"> to close the dialog box.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">You may also remove activities from the list shown in the Setup Activity Shortlist window by using the </text><text styleclass="Command" translate="true">Edit Selection</text><text styleclass="Body Text" translate="true"> command and then de-selecting those activities you wish to remove. (You de-select an activity by right-clicking the activity and then clicking the appropriate de-selection command.) However, it may be quicker to just right-click the activity you wish to remove and then click </text><text styleclass="Command" translate="true">Delete</text><text styleclass="Body Text" translate="true">.</text></para>
  </body>
</topic>
