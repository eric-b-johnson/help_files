﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Graph Reports Setup Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Graph Reports Setup Tab</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Setup tab allows you to configure the data that will appear in the graph as well as the type of graph that will be displayed. The tab contains the following:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Filter</text><text styleclass="Bullet L1" translate="true"> — This button displays the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Filter" domain="..\Common Commands\Common Commands.hmxp" styleclass="Bullet L1" translate="true">Filter</link><text styleclass="Bullet L1" translate="true"> dialog box, which allows you to restrict the data used in the graph before it is retrieved from the database.</text></li>
    </list>
    <list id="4" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">As of Date — This field allows you to use data from a particular time in the past (rather than current data) for the report. Note: This will only work if the tables on which the report is based are configured to support temporality.</text></li>
    </list>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Graph type set-up table — The table shown in the window shows the type of graph to be created as well as columns to select the data to be shown in the graph. One of five types of graphs may be selected:</text></li>
    </list>
    <list id="5" type="ul" listtype="bullet" formatstring="&#111;" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L2"><link displaytype="text" defaultstyle="true" type="topiclink" href="GraphType_General_Standalone" styleclass="Bullet L2" translate="true">General</link><text styleclass="Bullet L2" translate="true">.</text></li>
      <li styleclass="Bullet L2"><link displaytype="text" defaultstyle="true" type="topiclink" href="GraphType_Conditional_Standalone" styleclass="Bullet L2" translate="true">Conditional</link><text styleclass="Bullet L2" translate="true">.</text></li>
      <li styleclass="Bullet L2"><link displaytype="text" defaultstyle="true" type="topiclink" href="GraphType_Distribution_Standalone" styleclass="Bullet L2" translate="true">Distribution</link><text styleclass="Bullet L2" translate="true">.</text></li>
      <li styleclass="Bullet L2"><link displaytype="text" defaultstyle="true" type="topiclink" href="GraphType_General_Standalone" styleclass="Bullet L2" translate="true">Cumulative</link><text styleclass="Bullet L2" translate="true">.</text></li>
      <li styleclass="Bullet L2 Last"><link displaytype="text" defaultstyle="true" type="topiclink" href="GraphType_WeightedAvg_Standalone" styleclass="Bullet L2 Last" translate="true">Weight Average</link><text styleclass="Bullet L2 Last" translate="true">.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Depending on the type of graph selected, different columns will appear to configure the data for the graph. Use the links above for more information on each graph type.</text></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;Missing values in the </text><text styleclass="Text As Shown on Screen" translate="true">Category</text><text styleclass="L1 Indent" translate="true"> field and/or the </text><text styleclass="Text As Shown on Screen" translate="true">Colored By</text><text styleclass="L1 Indent" translate="true"> field are treated as an extra “N/A” group. However, if values are missing from the </text><text styleclass="Text As Shown on Screen" translate="true">Values</text><text styleclass="L1 Indent" translate="true"> field or the </text><text styleclass="Text As Shown on Screen" translate="true">Cond/Distr/Wgt By</text><text styleclass="L1 Indent" translate="true"> field, then the record is not used for the graph and its results.</text></para>
  </body>
</topic>
