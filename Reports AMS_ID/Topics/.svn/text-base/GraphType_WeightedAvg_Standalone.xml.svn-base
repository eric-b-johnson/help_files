﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Weighted Avg. Graph Type</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Weighted Avg. Graph Type</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Weighted Average type of graph plots any X-axis variable against a weighted average of a Y-axis value. This graph is similar to the General type of graph, with two exceptions:  (1) the aggregation function is always &quot;average&quot;; and (2) it is a weighted average, not a simple average, and so needs a &quot;weighting&quot; variable.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you select </text><text styleclass="Text As Shown on Screen" translate="true">Weighted Avg.</text><text styleclass="Body Text Before List" translate="true"> from the drop-down list in the </text><text styleclass="Text As Shown on Screen" translate="true">Graph Type</text><text styleclass="Body Text Before List" translate="true"> column, the graph set-up table displays the following columns to configure the Weighted Average graph type:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Category — This column provides a drop-down list that contains the available categories that will be displayed along the X axis of the graph. The system will partition the data by the category you select in this column.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Weighted Average Of — This column shows the column being averaged. A drop-down list is provided for you to select the column being averaged. This list contains the available &quot;value&quot; columns for the Y axis of the graph.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Colored By — This column provides a drop-down list that contains the available parameters to further partition the data selected in the </text><text styleclass="Text As Shown on Screen" translate="true">Category</text><text styleclass="Bullet L1" translate="true"> column. The data will be partitioned by category as well as by the parameter you select in this column. For example, if the Category value is Section and the Colored By value is Direction, then the graph will show colored bars for each direction for each section.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Weighted By — This column provides a drop-down list that contains the available parameters for use in calculating a weighted average. For example, for a length-weighted average of daily traffic, select Length from the drop-down list in this column (and select Daily Traffic from the drop-down list in the Weighted Average Of column).</text></li>
    </list>
  </body>
</topic>
