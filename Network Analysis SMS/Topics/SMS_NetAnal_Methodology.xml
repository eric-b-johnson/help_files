﻿<?xml version="1.0" encoding="UTF-8"?>
<topic template="Default" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Methodology for Performing SMS Network Analysis</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Methodology for Performing SMS Network Analysis</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The objective function determines what you optimize on and the analysis type. It varies for each objective choice as follows:</text></para>
    <para styleclass="Body Text"><table rowcount="4" colcount="3" style="cell-padding:3px; cell-spacing:0px; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid;">
      <tr style="vertical-align:top">
        <td style="width:116px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Objective (Function)</text></para>
        </td>
        <td style="width:135px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Analysis Type / Method</text></para>
        </td>
        <td style="width:263px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Description of Method</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:116px; height:14px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Priority</text></para>
        </td>
        <td style="width:135px; height:14px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Standard</text></para>
        </td>
        <td style="width:263px; height:14px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The application considers only one alternative per section / junction.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:116px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Index</text></para>
        </td>
        <td style="width:135px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">INCBEN</text></para>
        </td>
        <td rowspan="2" style="width:263px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The system considers up to three alternatives for each section / junction. The benefit ranking of each alternative is calculated using an incremental benefit cost ratio rather than a benefit cost ratio calculated only by comparison to the do-nothing alternative.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:116px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Benefit</text></para>
        </td>
        <td style="width:135px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">INCBEN</text></para>
        </td>
      </tr>
    </table></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">When compared to its threshold, the constraining function determines when to stop adding improvements to a year&apos;s work plan. Two constraining functions are provided: budget and analysis index.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Analysis methods are composed of combinations of:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Three objective types (Maximize Benefit, Index Value, and Prioritization), which also determines the type of analysis performed (either Standard or INCBEN).</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Two constraint types (Budget and Average Index Value).</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">This results in six different analysis methods. Each method differs from the other methods by the selection of the objective and constraint:</text></para>
    <list id="4" type="ul" listtype="bullet" formatstring="·" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The objective type determines the methodology used to calculate the gross benefit ranking for any potential projects.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The constraint type determines the criteria used to stop the selection of projects within a year of the analysis.</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">An outline of the analysis process for a particular scenario is as follows and is described in greater detail in the following sections:</text></para>
    <list id="5" type="ol" listtype="decimal" formatstring="%0:s." levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><link displaytype="text" defaultstyle="true" type="topiclink" href="Initialize_Analysis_SMS" styleclass="Bullet L1" translate="true">Initialize</link><text styleclass="Bullet L1" translate="true"> the analysis and analysis variables.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">For all sections/junctions, </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Est_AnalIndex_Trmt__SMS" styleclass="Bullet L1" translate="true">update</link><text styleclass="Bullet L1" translate="true"> the Project Selection Analysis Index for the assigned project(s). (This calculation varies by the selected objective type for the scenario.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">From the sections/junctions that remain, </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Establish_Candidate_WorkPlan_SMS" styleclass="Bullet L1" translate="true">establish</link><text styleclass="Bullet L1" translate="true"> this year’s candidate work program by selecting projects until the analysis constraint is met. (This calculation varies by the analysis constraint type selected.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">For all road sections/junctions in this year’s candidate work program, </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Improve_Condition_of_Sections_SMS" styleclass="Bullet L1" translate="true">improve</link><text styleclass="Bullet L1" translate="true"> the condition of the road and modify values of variables that change as a result of treatment application.</text></li>
      <li styleclass="Bullet L1 Last"><link displaytype="text" defaultstyle="true" type="topiclink" href="Continue_Process_for_NextYr_SMS" styleclass="Bullet L1 Last" translate="true">Repeat</link><text styleclass="Bullet L1 Last" translate="true"> Steps 2 through 5 for each successive year until the analysis period is over.</text></li>
    </list>
    <para styleclass="Table Column Head"></para>
    <para styleclass="Body Text"></para>
  </body>
</topic>
