﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Description of the Contractors Window</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Description of the Contractors Window</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Setup Contractor window contains the following panes:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The Contractors pane lists the names of the contractors and provides information about the contractors (such as contact person, address, and important telephone numbers). When you right-click a contractor&apos;s record, the shortcut menu provides the common commands along with </text><text styleclass="Command" translate="true">Edit Selection</text><text styleclass="Bullet L1" translate="true">.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">You use </text><text styleclass="Command" translate="true">Edit Selection</text><text style="font-family:&apos;Century Gothic&apos;; font-size:10pt; font-weight:bold; color:#000000;" translate="true">&#32;</text><text styleclass="L1 Indent" translate="true">to add entries to the Asset Types, Admin. Units, and Activities panes. After clicking </text><text styleclass="Command" translate="true">Edit Selection</text><text styleclass="L1 Indent" translate="true">, the first in a series of three dialog boxes opens. These dialog boxes display the hierarchies for asset types, administrative units, and work activities. You select (or de-select) the desired items and then click </text><text styleclass="Command" translate="true">OK</text><text styleclass="L1 Indent" translate="true"> to display the selected items in the lower panes. </text><link displaytype="text" defaultstyle="true" type="topiclink" href="HowtoAddaContractor" styleclass="L1 Indent" translate="true">Click here</link><text styleclass="L1 Indent" translate="true"> for more information.</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The Asset Types pane shows the asset types (for example, road surfaces and shoulders) on which the contractor selected in the Contractors pane may work.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The Activities pane displays the work activities that the selected contractor will perform for the selected asset type. Note that the activities available for selection are restricted to just those activities associated with the asset type. (See the Setup Asset Activity window to associate activities with an asset type.)</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The Admin. Units pane lists the administrative unit(s) that may assign the selected contractor to contracts for the asset type selected in the Asset Types pane. Note that the administrative units available for selection are restricted to the unit you selected when you logged on and any administrative units that are part of this administrative unit.</text></li>
    </list>
  </body>
</topic>
