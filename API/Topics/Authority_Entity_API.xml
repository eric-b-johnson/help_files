﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="helpproject.xsl" ?>
<topic template="Default" status="Complete" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="helpproject.xsd">
  <title translate="true">Authority</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Authority</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">You use the Authority API entity to retrieve authorities from an AgileAssets system. This entity allows only GET calls.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The following GET calls may be made to request data from the AgileAssets system via the API:</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves records for authorities in the system. Use the projection of a Q object to limit the number of returned columns. An example of this call is shown below:</text></para>
    <para styleclass="L1 Indent"><text styleclass="Input String" translate="true">http://vega:8080/ams_la/rest/v1/authority</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves authorities for the administrative unit identified by the ID {id}. Use the projection of a Q object to limit the number of returned columns. An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 01.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/name</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the name of a particular authority unit identified by the ID {id}. An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 02.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/labor</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves retrieves the labor inventory of a particular authority unit identified by the ID {id}. Use the projection of a Q object to limit the number of returned columns. An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 03.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/child</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the child units of a particular authority unit identified by the ID {id}. An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 04.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/parent</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the parent unit of a particular authority unit identified by the ID {id}. An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 05.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/user</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the users assigned to a particular authority unit identified by the ID {id}. An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 06.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/labor/shortlist</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the labor shortlist for a particular authority unit identified by the ID {id}. (Note: &#160;If {id} is omitted, the shortlists of all authority units is returned.) An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 07.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/equipment</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the equipment shortlist for a particular authority unit identified by the ID {id}. (Note: &#160;If {id} is omitted, the shortlists of all authority units is returned.) An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 08.PNG" scale="82.00%" width="26" height="26" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/material/shortlist</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the material shortlist for a particular authority unit identified by the ID {id}. (Note: &#160;If {id} is omitted, the shortlists of all authority units is returned.) An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 09.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{id}/project</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves projects that belong to particular owner identified by {id}. (If {id} is omitted, all projects of all authority units are returned.) An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 10.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{oid}/project/{pid}</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves a project identified by {pid} that belongs to particular owner identified by {oid}. (If {pid} is omitted, all projects for the identified owner are returned. If {oid} is omitted, all projects identified by {pid} are returned.) An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 11.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{oid}/project/{pid}/asset/{iid}</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the asset identified by {iid} associated with the project identified by {pid} that belongs to particular owner identified by {oid}. (The {iid}, {pid}, and {oid} terms are optional. If the {iid} term is omitted, all assets associated with project {pid} are returned. If {pid} is omitted, either asset(s) for all projects for the identified owner are returned. If {oid} is omitted, all assets for all projects identified are returned.) An example of this type is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 12.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">{baseURL}/authority/{oid}/project/{pid}/asset/{iid}/activity/{aid}</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">This call retrieves the asset identified by {iid} associated with the activity identified by {aid} performed in the project identified by {pid} that belongs to particular owner identified by {oid}. (The {aid}, {iid}, {pid}, and {oid} terms are optional.) An example of this type where all optional terms are included is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 13.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Another example where all optional terms other than asset ID are omitted is shown below:</text></para>
    <para styleclass="L1 Indent"><image src="API Authority Entity 14.PNG" scale="75.00%" width="24" height="24" styleclass="Image Caption"></image></para>
  </body>
</topic>
