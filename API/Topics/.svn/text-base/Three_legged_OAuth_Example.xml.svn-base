﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Example of Three-legged OAuth</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Example of Three-legged OAuth</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The following steps illustrate how to perform a three-legged OAuth authorization:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Obtain a request token by making a request via CURL:</text></li>
    </list>
    <para styleclass="L1 Indent"><image src="OAuth Two Leg Ex 01.PNG" scale="75.00%" width="414" height="88" styleclass="Image Caption"></image></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">The server responds as shown below:</text></para>
    <para styleclass="L1 Indent"><image src="OAuth Two Leg Ex 02.PNG" scale="75.00%" width="411" height="65" styleclass="Image Caption"></image></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Obtain an authorization token. The request is re-directed to the provider to collect additional information. The request should contain output from the server&apos;s response in Step 1.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">The server responds as shown below:</text></para>
    <para styleclass="L1 Indent"><image src="OAuth Three Leg Ex 01.PNG" scale="75.00%" width="284" height="54" styleclass="Image Caption"></image></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Obtain an access token by making a request via CURL:</text></li>
    </list>
    <para styleclass="L1 Indent"><image src="OAuth Three Leg Ex 02.PNG" scale="75.00%" width="448" height="113" styleclass="Image Caption"></image></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">The server responds as shown below:</text></para>
    <para styleclass="L1 Indent"><image src="OAuth Two Leg Ex 04.PNG" scale="75.00%" width="439" height="52" styleclass="Image Caption"></image></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Access a resource (the person identified by the User ID JEFF) by making a request via CURL:</text></li>
    </list>
    <para styleclass="L1 Indent"><image src="OAuth Two Leg Ex 05.PNG" scale="75.00%" width="440" height="100" styleclass="Image Caption"></image></para>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">The server responds as shown below:</text></para>
    <para styleclass="L1 Indent"><image src="OAuth Two Leg Ex 06.PNG" scale="75.00%" width="444" height="302" styleclass="Image Caption"></image></para>
  </body>
</topic>
