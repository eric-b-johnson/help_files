﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" status="Complete" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">OAuth Configuration</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">OAuth Configuration</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Configuration is accomplished in three steps as described below.</text></para>
    <para styleclass="Internal Heading"><text styleclass="Internal Heading" translate="true">Step 1: &#160;Add Consumer</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The first step is to register a new consumer in the AgileAssets system in the OAuth Security window. (This window is typically launched from the Setup menu in the System module.) In this window, you enter the public key and the consumer secret that your client will use when making requests to the Web API as shown in the example below. After you have saved the information, ensure that OAuth authentication is enabled (that is, that a check mark appears in the </text><text styleclass="Text As Shown on Screen" translate="true">Enabled</text><text styleclass="Body Text" translate="true"> column).</text></para>
    <para styleclass="Body Text"><image src="OAuth Window.PNG" scale="60.00%" width="889" height="124" styleclass="Image Caption"></image></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;The consumer secret is encrypted on the database side. For security reason the secret is never decrypted on the user-interface sided. When updating the secret, you must change it completely. If “Client Authorization Flow” is enabled, the client will be re-directed to an authorization page where the end user may give permission to the client application to log in.</text></para>
    <para styleclass="Internal Heading"><text styleclass="Internal Heading" translate="true">Step 2: Configure the Application’s web.xml</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">To activate API Security, you must ensure that the Init parameter </text><text styleclass="Text As Shown on Screen" translate="true">com.sun.jersey.spi.container.ContainerRequestFilters</text><text styleclass="Body Text" translate="true"> is set for the RestService as shown in the example below.</text></para>
    <para styleclass="Body Text"><image src="OAuth Config Ex.png" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Internal Heading"><text styleclass="Internal Heading" translate="true">Step 3: &#160;Configure the Client</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The client will require the following information to make authentication requests to the Web API:</text></para>
    <para styleclass="Body Text"><table styleclass="Default" rowcount="6" colcount="2" style="cell-padding:4px; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid;">
      <tr style="vertical-align:top">
        <td style="width:180px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">OAuth Config</text></para>
        </td>
        <td style="width:285px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Value</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:180px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Request Token URL</text></para>
        </td>
        <td style="width:285px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">WMS_BASE_URL + &quot;/rest/v1/token/request&quot;</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:180px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Authorization URL</text></para>
        </td>
        <td style="width:285px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">WMS_BASE_URL + &quot;/Kernel/oauth_authorization.jsp&quot;&quot;</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:180px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Access Token URL</text></para>
        </td>
        <td style="width:285px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">WMS_BASE_URL + &quot;/rest/v1/token/access&quot;</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:180px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">OAuth Signing Types</text></para>
        </td>
        <td style="width:285px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">PLAINTEXT or HMAC-SHA1</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:180px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Consumer Key</text></para>
        </td>
        <td style="width:285px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">As configured in Step 1</text></para>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
