﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">The Columns Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">The Columns Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; font-weight:normal; font-style:normal; text-transform:none; vertical-align:baseline; color:#ff0000; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">Note: &#160;The values set in this pane for the base LRM should never be changed without first consulting AgileAssets.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Columns pane shows the columns that define the reference system selected in the upper pane. The following columns appear in this pane:</text></para>
    <list id="35" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Column ID — This column shows the internal column ID used in the system.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Column ID &quot;To&quot; — This column shows the internal column ID used in the system. The contents of this field are not used if the Show &quot;To&quot; check box is clear.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Column Label — This columns shows the column heading label that appears for the column throughout the system.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Column Label &quot;To&quot; — This column shows the column heading label that appears for this column throughout the system. The contents of this field are not used if the Show &quot;To&quot; check box is clear.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Hidden — When this check box is selected, the column will not be displayed.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Initial Value — If a value appears in this column, then the value will be automatically set to any record that is location-referenced when it is inserted into the system.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">LRS Column Type — This column indicates whether the column is the &quot;location measure&quot; column. In defining an alternate reference system, one and only one column must be set as the &quot;location measure&quot;; all other columns must be set as &quot;location attribute&quot;.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Order — This column indicates the order (left to right) in which the columns are seen in data windows throughout the system.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Show &quot;To&quot;? — If the check box in this column is checked, then this LRM field will be shown twice in any location-referenced window when an alternate LRM is selected. It is shown once for the &quot;from&quot; point and then again for the &quot;to&quot; point. If clear, then the LRM field appears once and applies to both &quot;from&quot; and &quot;to&quot; points. </text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;This column must be checked for the column designated &quot;location measure.&quot;</text></para>
  </body>
</topic>
