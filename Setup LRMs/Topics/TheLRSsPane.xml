﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">The LRSs Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">The LRSs Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The LRSs pane shows the base reference system (as denoted by a shadow check mark in the </text><text style="font-family:Tahoma; font-size:10pt; color:#000000;" translate="true">Basic LRS?</text><text styleclass="Body Text" translate="true"> column) along with a record for each alternate reference system. The text entered in the </text><text style="font-family:Tahoma; font-size:10pt; color:#000000;" translate="true">Loc Ref Name</text><text styleclass="Body Text" translate="true"> column in the LRSs pane is what will appear in the drop-down list in the </text><text style="font-family:Tahoma; font-size:10pt; color:#000000;" translate="true">Location Reference</text><text styleclass="Body Text" translate="true"> field on the side of the browser window.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Each reference system is assigned a type in the </text><text style="font-family:Tahoma; font-size:10pt; color:#000000;" translate="true">LRS Type</text><text styleclass="Body Text Before List" translate="true"> column, with the type being selected from a drop-down list. The list provides the following selections:</text></para>
    <list id="45" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Use Location Across — This selection means that for this reference system the identification of a position laterally (for example, across the road) will be done using the two columns Lane Direction and Lane Number. (These two columns are pre-defined in the system, and appear in various windows throughout the system as indicated in the table below.)</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;The base LRM is always set to &quot;Use Location Across,&quot; thus providing the capability of lateral positioning, which can then be employed (if desired) for a particular system implementation.</text></para>
    <list id="45" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Ignore Location Across — This selection means that for this reference system the identification of a position laterally does not use the two pre-defined columns Lane Direction and Lane Number. See the table below for its affect in windows throughout the system.</text></li>
    </list>
    <para styleclass="L1 Indent"><table rowcount="4" colcount="3" style="width:auto; cell-padding:3px; cell-spacing:0px; page-break-inside:avoid; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid; background-color:none; head-row-background-color:none; alt-row-background-color:none;">
      <tr style="vertical-align:top">
        <td style="width:170px;">
          <para styleclass="Table Column Head"></para>
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Window Type/Situation</text></para>
        </td>
        <td style="width:230px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">What Occurs When &quot;Use Location Across&quot; Is Selected for an Alternate LRM</text></para>
        </td>
        <td style="width:236px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">What Occurs When &quot;Ignore Location Across&quot; Is Selected for an Alternate LRM</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:170px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">In the Alternate LRM Definition window.</text></para>
        </td>
        <td style="width:230px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The Lane Direction column is shown.</text></para>
        </td>
        <td style="width:236px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">No lateral indication columns are shown.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:170px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">In any location-referenced window when an alternate LRM is selected in the Location Reference field.</text></para>
        </td>
        <td style="width:230px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The Lane Direction and Lane Number columns are shown.</text></para>
        </td>
        <td style="width:236px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">No lateral indication columns are shown.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:170px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">In any location-referenced window when the base LRM is selected in the Location Reference field.</text></para>
        </td>
        <td style="width:230px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The Lane Direction and Lane Number columns are shown.</text></para>
        </td>
        <td style="width:236px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">The Lane Direction and Lane Number columns are shown. (This is because the base LRM is always set to &quot;Use Location Across.&quot;)</text></para>
        </td>
      </tr>
    </table></para>
    <para styleclass="Normal"></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click an alternate reference system in the upper pane, the system displays the following special command in a shortcut menu:</text></para>
    <list id="46" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Make Window</text><text styleclass="Bullet L1" translate="true"> — This command displays a new window that shows the tree view of the menu items of the system. In the new window, click the parent menu item into which a menu item for the window that will show the cross-reference table between the Basic Reference System (BRS) and the selected alternate systems. After selecting the parent menu item, click </text><text styleclass="Command" translate="true">OK</text><text styleclass="Bullet L1" translate="true"> to close the window. Then log off and log back on, and the menu item will be in the designated place, with the name of the menu item being what is entered in the </text><text styleclass="Text As Shown on Screen" translate="true">Loc Ref Name</text><text styleclass="Bullet L1" translate="true"> column. Open the new window to see the cross-reference table. The BRS is shown on the left and the alternate system is on the right.</text></li>
    </list>
    <para styleclass="Normal"></para>
  </body>
</topic>
