﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Add a New Layer</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Add a New Layer</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">To add a new layer to an existing contract:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Display the Construction History window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Construction History pane, click the record showing the road section to which layer information will be added. The system will highlight it to show that it is selected.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Layers pane, right-click and then click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">. A new record is added to the table.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the new record in the Layers pane, click in the </text><text styleclass="Text As Shown on Screen" translate="true">Layer</text><text styleclass="Bullet L1" translate="true"> column and then type the ID code for the layer. The value entered for a layer ID may be any two-digit number (other than 99) that designates the vertical position of the layer, with the smallest number indicating the top of the layer.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Material Code</text><text styleclass="Bullet L1" translate="true"> column, click the down arrow and then click the material for the layer.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Thickness</text><text styleclass="Bullet L1" translate="true"> column, type the thickness of the layer . (For milling, enter a negative value.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Repeat steps 3 through 6 for any additional layers.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">When all layers are entered, click the </text><image src="Icon_Save.png" scale="100.00%" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1 Last" translate="true"> icon to save the new information.</text></li>
    </list>
    <para styleclass="Normal"></para>
  </body>
</topic>
