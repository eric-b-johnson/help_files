﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Rebuild Automatically Recommended Projects</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Rebuild Automatically Recommended Projects</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Projects are divided into three types: Recommended, Main, and Secondary. The first column of the Projects pane shows &#160;what type is assigned to a project. Main and Secondary projects are built and edited by the user. &#160;Recommended projects are built (and rebuilt) automatically -- and are applied to road sections and junctions using the “SMS ProactiveModelBasis + Makes Recommended Projects” system job.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Note: &#160;Since Recommended projects are built automatically, any manually made adjustments to these type of projects or their treatments will be lost when the system job is run. Consequently, if you want to save changes that were made to a Recommended project, perform the following steps:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Insert a new project record in the Projects pane by right-clicking the pane and then clicking </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the first column of the new record, select either the Main or Secondary type.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Projects pane, click the record for the Recommended project to select it.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Treatments pane, select all treatment records by clicking the first record, holding down the Shift key, and clicking the last record.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Right-click the selected records and then click </text><text styleclass="Command" translate="true">Copy Selected Items</text><text styleclass="Bullet L1" translate="true">.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Projects pane, click the record for the new project to select it.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Treatments pane, right-click and then click </text><text styleclass="Command" translate="true">Paste</text><text styleclass="Bullet L1" translate="true">. The treatment records copied from the Recommended project appear in the pane.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">If necessary, make an further edits for the new project.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Save the new project by clicking the </text><image src="Icon_Save.PNG" scale="100.00%" styleclass="Image Caption"></image><text styleclass="Bullet L1" translate="true"> icon.</text></li>
    </list>
  </body>
</topic>
