﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">LOS Plan Details</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">LOS Plan Details</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">For the plan selected in the LOS Plans pane, this pane lists all defects addressed by the plan and the costs for remedying each defect. </text></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><image src="hmtoggle_plus0.gif" scale="100.00%" styleclass="Internal Heading"></image><tab /><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="false" translate="true" src-collapsed="hmtoggle_plus0.gif" src-expanded="hmtoggle_plus1.gif" styleclass="Internal Heading"><caption translate="true"><![CDATA[Description of the Columns]]></caption></toggle></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">This pane provides the following columns:</text></para>
          <list id="196" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Current Cost — This column is the total amount of money expended during the year selected in the </text><text styleclass="Text As Shown on Screen" translate="true">Fiscal Year for Accomplishments</text><text styleclass="Bullet L1" translate="true"> column in the Master Work Plan Types pane on activities intended to remedy the defect.</text></li>
          </list>
          <list id="197" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Current LOS — This column shows the current LOS grade.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Defect — This column shows the name of the defect.</text></li>
          </list>
          <list id="196" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Functional Class — This column shows the type of road (interstate, primary, etc.) associated with the defect.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Scenario Cost — This column shows the predicted amount of money required to attain the LOS grade selected in the </text><text styleclass="Text As Shown on Screen" translate="true">Defect Grade</text><text styleclass="Bullet L1" translate="true"> column. The difference between the Scenario Cost and the Current Cost is the amount of additional money required to change the Current LOS grade to the desired LOS grade.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Station — This column shows the name of the administrative unit that performs activities to remedy the defect. </text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Target LOS — This column shows the desired LOS grade for this defect. </text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: When you change the grade for a defect, the system automatically re-calculates the Scenario Cost.</text></para>
        </td>
      </tr>
    </table></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><image src="hmtoggle_plus0.gif" scale="100.00%" styleclass="Internal Heading"></image><tab /><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="false" translate="true" src-collapsed="hmtoggle_plus0.gif" src-expanded="hmtoggle_plus1.gif" styleclass="Internal Heading"><caption translate="true"><![CDATA[Description of the Right-click Shortcut Menu Commands]]></caption></toggle></para>
    <para styleclass="Internal Heading" style="text-indent:-18px; margin-left:18px;"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click the pane, the system displays a shortcut menu with the common commands along with the following special commands:</text></para>
          <list id="198" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Copy from Defect Survey</text><text styleclass="Bullet L1 Last" translate="true"> — </text><text styleclass="Bullet L1" translate="true">This command allows you to copy one or more defect records from the &#160;defect survey identified by the </text><text styleclass="Text As Shown on Screen" translate="true">Defect Survey Year </text><text styleclass="Bullet L1" translate="true">column to the current plan. When you select this command the system displays a dialog box so you may select the defect records to be copied.</text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: Only defect records that do not exist in the current plan will be copied.</text></para>
          <list id="198" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Copy from Other Type</text><text styleclass="Bullet L1" translate="true"> — This command allows you to copy one or more defect records from &#160;an &#160;LOS plan to the current LOS plan. When you select this command the system displays a dialog box so you may select the LOS plan and defect records to be copied. </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Copy_MWP_Defect_Records" styleclass="Bullet L1" translate="true">Click here</link><text styleclass="Bullet L1" translate="true"> for more information.</text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: Only defect records that do not exist in the current plan will be copied.</text></para>
          <list id="199" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Copy MMS Scenario Results</text><text styleclass="Bullet L1" translate="true"> — This command allows you to copy the results from the results &#160;of a maintenance analysis to the current plan.</text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: Only defect records that do not exist in the current plan will be copied.</text></para>
          <list id="198" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Fill Scenario Costs</text><text styleclass="Bullet L1" translate="true"> — This command calculates the scenario costs for the defects.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Show Total Budget</text><text styleclass="Bullet L1" translate="true"> — This command adds all values in the </text><text styleclass="Text As Shown on Screen" translate="true">Scenario Cost</text><text styleclass="Bullet L1" translate="true"> column and displays the total in a popup window.</text></li>
          </list>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
