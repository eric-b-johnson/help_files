﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Make a Graph Window </title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Make a Graph Window </text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Rather than create a window that displays data in a tabular format, you may also display data as a graph along a route (provided the underlying table in the database contains location information). This is accomplished as follows:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Display the Tables window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Locate the table for which you will create a window. Right-click the record showing the table and then </text><text styleclass="Command" translate="true">Make Window</text><text styleclass="Bullet L1" translate="true">. &#160;The application displays a new window. The default selection in the new window is to create a data window, not a graph window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">For the first option (</text><text styleclass="Text As Shown on Screen" translate="true">Main</text><text styleclass="Bullet L1" translate="true">), click the down arrow to display the list of types of windows and then click the </text><text styleclass="Text As Shown on Screen" translate="true">Graph Along the Route Window</text><text styleclass="Bullet L1" translate="true"> entry.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click </text><text styleclass="Command" translate="true">Next</text><text styleclass="Bullet L1" translate="true">. The application displays a second new window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the second dialog box, check that the window title is what you want. If necessary, modify the title as desired.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">If the data in the window may be modified, click the</text><text styleclass="Text As Shown on Screen" translate="true"> Editable?</text><text styleclass="Bullet L1" translate="true"> check box to select it.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the lower part of the dialog box, expand the menu hierarchy to locate the parent menu item under which the menu item to display the window will be placed.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Right-click the parent menu item and then click </text><text styleclass="Command" translate="true">Add Branch</text><text styleclass="Bullet L1" translate="true">. The application displays a new dialog box so you may enter the name of the menu item.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the dialog box, in the </text><text styleclass="Text As Shown on Screen" translate="true">New Menu ID</text><text styleclass="Bullet L1" translate="true"> field enter the internal ID for the menu item that will open the window. (This ID should be lower case and not contain spaces.) Also, in the </text><text styleclass="Text As Shown on Screen" translate="true">New Menu Name</text><text styleclass="Bullet L1" translate="true"> field, enter the name of the menu item that will open the window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click </text><text styleclass="Command" translate="true">OK</text><text styleclass="Bullet L1" translate="true"> to close the dialog box. A new node is added subordinate to the parent menu item.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Right-click the new node and then click </text><text styleclass="Command" translate="true">Select This</text><text styleclass="Bullet L1" translate="true">. The system places a check mark in the square beside the menu name to denote that it is selected.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click </text><text styleclass="Command" translate="true">OK</text><text styleclass="Bullet L1" translate="true"> to make the window.</text></li>
    </list>
  </body>
</topic>
