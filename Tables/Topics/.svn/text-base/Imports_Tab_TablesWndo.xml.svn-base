﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Imports Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Imports Tab</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;The </text><text styleclass="Command" translate="true">Insert Like</text><text styleclass="Body Text" translate="true"> command creates a new import configuration and copies fields from </text><text styleclass="Body Text" style="font-weight:bold;" translate="true">both</text><text styleclass="Body Text" translate="true"> the Imports and Import Detail tabs.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Imports tab of the Import Data window lists all import routines that have been configured to import data into the table selected in the Tables window. The left pane lists the import routines. </text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: In the left pane, the </text><text styleclass="Text As Shown on Screen" translate="true">Is Public</text><text styleclass="Body Text" translate="true"> check box determines whether the routine will be available in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Import_Runner" domain="..\Import Runner\Import Runner.hmxp" styleclass="Body Text" translate="true">Import Runner</link><text styleclass="Body Text" translate="true"> window. When this check box is selected, the routine appears in the Import Runner window and may be run from that window rather than the Tables window.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The right pane provides the fields for configuring the general aspects of the import routine selected in the left pane. This pane also provides fields in the </text><text styleclass="Text As Shown on Screen" translate="true">Options</text><text styleclass="Body Text Before List" translate="true"> box to halt the import if desired. When halted, the system rolls back all changes since the last commit to the database. The fields in the </text><text styleclass="Text As Shown on Screen" translate="true">Options</text><text styleclass="Body Text Before List" translate="true"> box are described below:</text></para>
    <list id="7" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">When the </text><text styleclass="Text As Shown on Screen" translate="true">Exit on Exception</text><text styleclass="Bullet L1" translate="true"> check box is selected, then the system will stop the import process when the number of exceptions entered in the </text><text styleclass="Text As Shown on Screen" translate="true">Exception Limit</text><text styleclass="Bullet L1" translate="true"> field is reach (or the first exception if this field is left blank).</text></li>
    </list>
    <list id="8" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Regardless of the setting of </text><text styleclass="Body Text" translate="true">the </text><text styleclass="Text As Shown on Screen" translate="true">Exit on Exception</text><text styleclass="Body Text" translate="true"> check box, the</text><text styleclass="Bullet L1" translate="true">&#32;</text><text styleclass="Text As Shown on Screen" translate="true">Commit Interval</text><text styleclass="Bullet L1" translate="true"> field determines when records are committed to the database based on the number of records processed. For example, if this field is set to 1000, then after the 1,000th record is processed the system will commit the last 1,000 records to the database.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;Once records are committed to the database, an exception will not cause those records to be restored to the condition they were in before the import process began.</text></para>
    <list id="9" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">If the </text><text styleclass="Text As Shown on Screen" translate="true">Auto. Split Partially Subord. Sections</text><text styleclass="Bullet L1" translate="true"> check box is selected, then the system will split the data that crosses a concurrency (subordinate) boundary. (This check box only has an effect for purely location-based data.) If this check box is not selected, then an error will result when the system attempts to enter data that crosses a concurrency boundary.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">If the </text><text styleclass="Text As Shown on Screen" translate="true">Save Errors Only</text><text styleclass="Bullet L1 Last" translate="true"> check box is selected, then the system will save the text describing exceptions to a log file.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The fields that are displayed in the right pane are dependent on the selected Source Type. The following table lists the different types of sources from which data may be imported and the fields that become available when you select a particular source type. Following the table is a description of the different fields that may appear in the right pane.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;For the Database Table and SQL Server Table source types, the data source is defined in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Data_Source_Connection" domain="..\Data Source Connection\Data Source Connection.hmxp" styleclass="Body Text" translate="true">Data Source Connection</link><text styleclass="Body Text" translate="true"> window. If this window contains more than one source, the system will display a dialog box so you may select the desired source.</text></para>
    <para styleclass="Body Text"><table styleclass="Default" rowcount="9" colcount="8" style="cell-padding:4px; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid;">
      <tr style="vertical-align:bottom">
        <td style="width:91px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Source Type</text></para>
        </td>
        <td style="vertical-align:bottom; width:55px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Select Source</text></para>
        </td>
        <td style="vertical-align:bottom; width:78px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Data Source Where Clause</text></para>
        </td>
        <td style="vertical-align:bottom; width:56px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Import Method</text></para>
        </td>
        <td style="vertical-align:bottom; width:46px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Where Clause</text></para>
        </td>
        <td style="vertical-align:bottom; width:72px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">After Each Import Script</text></para>
        </td>
        <td style="vertical-align:bottom; width:80px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">After All Import Scripts</text></para>
        </td>
        <td style="vertical-align:bottom; width:45px;">
          <para styleclass="Table Column Head" style="text-align:center;"><text styleclass="Table Column Head" translate="true">Has Header</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Access file</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Comma separated</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Database table</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Dbf file</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Excel file</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Fixed length</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">SQL server table</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:46px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:91px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Shape file</text></para>
        </td>
        <td style="vertical-align:middle; width:55px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:78px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
        <td style="vertical-align:middle; width:56px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="width:46px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="vertical-align:middle; width:72px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:80px;">
          <para styleclass="Table Body Text" style="text-align:center;"><text styleclass="Table Body Text" translate="true">x</text></para>
        </td>
        <td style="vertical-align:middle; width:45px;">
          <para styleclass="Table Body Text" style="text-align:center;"></para>
        </td>
      </tr>
    </table></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The fields are </text><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="true" translate="true" styleclass="Body Text Before List"><caption translate="true"><![CDATA[described below:]]></caption></toggle></para>
    <para styleclass="Body Text Before List"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <list id="10" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">After All Import Script — This field contains a drop-down list of </text><text styleclass="Text As Shown on Screen" translate="true">After All Import</text><text styleclass="Bullet L1" translate="true"> Groovy scripts. If desired, you may select one to run at the very end of the import routine.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">After Each Import Script — This field contains a drop-down list of </text><text styleclass="Text As Shown on Screen" translate="true">After Each Import</text><text styleclass="Bullet L1" translate="true"> Groovy scripts. If desired, you may select one to run after each record is imported.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Data Source Where Clause — If you would like to restrict the data imported from the data source, you may enter an SQL statement in this field to identify the records you want to import.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Has Header — When this check box is selected, the first record of the data source is a header record.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Import Method — This field provides the following radio buttons to select what will happen to the records in the target table:</text></li>
          </list>
          <list id="11" type="ul" listtype="bullet" formatstring="&#111;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">The </text><text styleclass="Text As Shown on Screen" translate="true">Replace</text><text styleclass="Bullet L2" translate="true"> method truncates the target table and then begins inserting new records from the data source.</text></li>
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">The </text><text styleclass="Text As Shown on Screen" translate="true">Append</text><text styleclass="Bullet L2" translate="true"> method does not truncate the target table; new records are simply inserted at the end of the existing target table.</text></li>
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">Update</text><text styleclass="Bullet L2" translate="true"> method, the import routine looks for an existing record in the target table that matches a record in the data source and then updates the target table&apos;s record with data from the data source&apos;s record. (If a record is not found in the target table, the record in the data source is ignored.)</text></li>
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">The </text><text styleclass="Text As Shown on Screen" translate="true">Insert &amp; Update</text><text styleclass="Bullet L2" translate="true"> method is the same as the </text><text styleclass="Text As Shown on Screen" translate="true">Update</text><text styleclass="Bullet L2" translate="true"> method except if a record is not found in the target table, then the record is added to the target table (rather than being ignored).</text></li>
          </list>
          <para styleclass="L2 Indent"><text styleclass="L2 Indent" translate="true">Note: &#160;When you select either the </text><text styleclass="Text As Shown on Screen" translate="true">Update</text><text styleclass="L2 Indent" translate="true"> or </text><text styleclass="Text As Shown on Screen" translate="true">Insert &amp; Update</text><text styleclass="L2 Indent" translate="true"> method, an additional field is added to the pane called </text><text styleclass="Text As Shown on Screen" translate="true">Update by Index</text><text styleclass="L2 Indent" translate="true">. This is a drop-down list that contains the different means by which a record may be identified.</text></para>
          <list id="10" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Loc Ref — This field provides a drop-down list of the available location reference systems. You select the LRS appropriate for the data records being imported.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Select Source</text><text styleclass="Bullet L1" translate="true"> — This button allows you to select the source that contains the data to be imported. When you click the button, the application displays a new window with the sources that are appropriate to the selected source type. (For example, selecting </text><text styleclass="Text As Shown on Screen" translate="true">Access file</text><text styleclass="Bullet L1" translate="true"> as the source type would cause all Microsoft Access database files [*.mdb] to be displayed when you click the </text><text styleclass="Command" translate="true">Select Source</text><text styleclass="Bullet L1" translate="true"> button.)</text></li>
          </list>
          <para style="margin-bottom:5px; margin-left:48px;"><text styleclass="L1 Indent" translate="true">Note: &#160;The location where the application looks for files is set at the time of configuration. When the project manager (typically) enters a root folder in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Setup_File_System" domain="..\Setup File System\Setup File System.hmxp" styleclass="L1 Indent" translate="true">Setup File System</link><text styleclass="L1 Indent" translate="true"> window, then what folders and files under that root folder will be available depends on whether your user ID has an administrative unit assigned to it in the User Names and Access window:</text></para>
          <list id="12" type="ul" listtype="bullet" formatstring="&#111;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L2"><text styleclass="Bullet L2" translate="true">If &#160;the </text><text styleclass="Text As Shown on Screen" translate="true">Administrative Unit</text><text styleclass="Bullet L2" translate="true"> field in the User Names and Access window is blank, then you will see all folders and files under that root folder.</text></li>
          </list>
          <list id="13" type="ul" listtype="bullet" formatstring="&#111;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L2 Last" style="font-family:&apos;Courier New&apos;; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L2 Last"><text styleclass="Bullet L2 Last" translate="true">If </text><text styleclass="Bullet L2" translate="true">the </text><text styleclass="Text As Shown on Screen" translate="true">Administrative Unit</text><text styleclass="Bullet L2" translate="true"> field in the User Names and Access window</text><text styleclass="Bullet L2 Last" translate="true"> is filled, then you will only see a file folder and its contents under that root folder that was especially made for your administrative unit. Ask your system administrator for that subfolder&apos;s name. (Note to System Administrator: &#160;The file folder name is the internal ID of the administrative unit.)</text></li>
          </list>
          <list id="10" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Where Clause — When you select the </text><text styleclass="Text As Shown on Screen" translate="true">Insert</text><text styleclass="Bullet L1 Last" translate="true"> import method, you may enter an SQL statement in this field to identify what records are deleted from the target table.</text></li>
          </list>
        </td>
      </tr>
    </table></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">After the details of the import are set in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Import_Detail_Tab_TablesWndo" styleclass="Body Text" translate="true">Import Detail</link><text styleclass="Body Text" translate="true"> tab, the </text><text styleclass="Command" translate="true">Import</text><text styleclass="Body Text" translate="true"> button at the bottom of the Import Data window will execute the import selected in the Target Table pane of the Imports tab. Instead of running the import immediately, you may instead click the </text><text styleclass="Command" translate="true">Close</text><text styleclass="Body Text" translate="true"> button, which saves the changes to the import definition so that the import may be run at a later time from the Tables window or the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Import_Runner" domain="..\Import Runner\Import Runner.hmxp" styleclass="Body Text" translate="true">Import Runner</link><text styleclass="Body Text" translate="true"> window (if the </text><text styleclass="Text As Shown on Screen" translate="true">Is Public</text><text styleclass="Body Text" translate="true"> check box is selected for the routine).</text></para>
  </body>
</topic>
