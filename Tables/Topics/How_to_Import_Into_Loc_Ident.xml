﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Import Into Loc_Ident</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Import Into Loc_Ident</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When a table to be imported contains the column LOC_IDENT, then the Import Column Mapping pane of the Import Details tab automatically includes all “location” columns, namely:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">LOC_IDENT</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">ROUTE_ID</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">LANE_DIR</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">LANE_ID</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">PERPENDICULAR_OFFSET</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">OFFSET_FROM</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">OFFSET_TO</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">To identify “location” there should be a mapping to each of these columns from the source data. For example, if the source data had the normal system route name and mile points, L/R for direction, no lanes, and no perpendicular offset, then the mapping would be as shown in the table below:</text></para>
    <para styleclass="Body Text"><table styleclass="Default" rowcount="8" colcount="4" style="width:auto; cell-padding:4px; border-width:2px; border-spacing:0px; border-collapse:collapse; cell-border-width:1px; border-color:#000000; border-style:solid;">
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Source</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">ImportMapping Groovy Script</text></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Target</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Column Head"><text styleclass="Table Column Head" translate="true">Notes</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Max loc_ident</text></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">LOC_IDENT</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Always use this ImportMapping Groovy script to set LOC_IDENT.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Route</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Map by column name</text></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">ROUTE_ID</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Direction</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Set direction for MDTPMS</text></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">LANE_DIR</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">R maps to 1; L maps to 2; B maps to 0; otherwise reject the record.</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">0</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">LANE_ID</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">0</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">PERPENDICULAR_OFFSET</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">From</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Set to column value</text></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">OFFSET_FROM</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:62px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">To</text></para>
        </td>
        <td style="width:167px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">Set to column value</text></para>
        </td>
        <td style="width:138px;">
          <para styleclass="Table Body Text"><text styleclass="Table Body Text" translate="true">OFFSET_TO</text></para>
        </td>
        <td style="width:210px;">
          <para styleclass="Table Body Text"></para>
        </td>
      </tr>
    </table></para>
  </body>
</topic>
