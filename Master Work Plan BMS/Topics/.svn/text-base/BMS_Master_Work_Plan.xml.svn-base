﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">BMS Master Work Plan</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">BMS Master Work Plan</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;The data displayed in this window is not affected by filtering. To facilitate locating a particular record, you may sort the records (either by double-clicking a column head or using the right-click </text><text styleclass="Command" translate="true">Sort</text><text styleclass="Body Text" translate="true"> command) or use the right-click </text><text styleclass="Command" translate="true">Find</text><text styleclass="Body Text" translate="true"> command.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">All transportation agencies keep a list of planned projects, which is updated regularly for when the project will be performed, where it will be performed, the intended treatment, and estimated project cost. This list is termed a work plan, and it constitutes the final list of structures proposed for treatment based upon analysis results and, as necessary, user intervention. The system allows you to develop multiple work plans, with the different work plans being defined in the Work Plan Type window and the data for each work plan being entered in this window. The data is then used in optimization analysis and reports.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Master Work Plan window provides a different way to group projects (that is, by work plan) than the way projects are grouped in the Project Composition window. The Project Composition window is where all candidate projects and their constituent treatments are built and where expected project cost, benefit, priority, and improvement are calculated. These candidate projects are used as input to analysis in the Optimization Analysis window.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The projects shown in the Master Work Plan window are the same as the ones in the Project Composition window. Any project inserted in the Master Work Plan window will also be shown in the Project Composition window. Similarly, any edits to projects in this window will be reflected in the Project Composition window.</text></para>
  </body>
</topic>
