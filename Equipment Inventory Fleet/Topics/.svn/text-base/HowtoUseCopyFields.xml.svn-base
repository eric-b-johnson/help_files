﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Use the Copy Fields Command</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Use the Copy Fields Command</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">You use the </text><text styleclass="Command" translate="true">Copy Fields</text><text styleclass="Body Text" translate="true"> command to copy the characteristics from one vehicle record to all other vehicle records in the same vehicle group. (A vehicle group is typically all vehicles of the same make, model, and year.) </text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;A vehicle group must first be defined in the Vehicle Group configuration table before being able to use this command.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Once the vehicle group is established, you assign all like vehicles to this group:</text><text styleclass="Normal" style="font-size:5pt;" translate="true">&#32;</text></para>
    <list id="2" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Inventory pane of the Inventory tab of the Equipment Inventory window, locate the record of the first vehicle to be placed in the vehicle group.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the</text><text styleclass="Text As Shown on Screen" translate="true"> Vehicle Group</text><text styleclass="Bullet L1" translate="true"> column of the located record, click the down arrow to display the drop-down list and then click the vehicle group to which this vehicle belongs.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Locate all other vehicles in the Inventory pane that are in the same vehicle group and set the </text><text styleclass="Text As Shown on Screen" translate="true">Vehicle Group</text><text styleclass="Bullet L1 Last" translate="true"> column of each record to be the same vehicle group as selected in step 2.</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">Now that all like vehicles are assigned to the same vehicle group, enter the characteristics for one vehicle in the group and then copy the characteristics to all other records:</text><text styleclass="Normal" style="font-size:5pt;" translate="true">&#32;</text></para>
    <list id="3" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the record for one of the vehicles in the vehicle group, enter the vehicle characteristics in the appropriate columns of the record.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Point to the record of step 1, right-click, and then click </text><text styleclass="Command" translate="true">Copy Fields</text><text styleclass="Bullet L1" translate="true">. The system displays a dialog box.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the dialog box, select the fields that will be copied.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">After selecting the fields, click the </text><text styleclass="Command" translate="true">OK</text><text styleclass="Bullet L1" translate="true"> button. The system closes the dialog box and copies the selected fields from the selected record to all other records in the same vehicle group.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Click &#160;</text><image src="Icon_Save.png" scale="90.00%" width="19" height="19" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1 Last" translate="true">.</text></li>
    </list>
  </body>
</topic>
