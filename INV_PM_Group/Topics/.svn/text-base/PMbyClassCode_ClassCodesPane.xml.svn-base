﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Class Codes Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Class Codes Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The left pane, Class Codes, shows a tree view of the classification codes for the asset type. A &apos;+&apos; sign will expand the hierarchy, while a &apos;–&apos; sign will contract the hierarchy. (If no sign is shown, then this node is the lowest node in the hierarchy and cannot be contracted or expanded further.) </text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">A check mark in the square beside the name of a class code </text><text styleclass="Bullet L1 Last" translate="true">indicates that it has its own PM schedule. If the square is clear, then the class code inherits its PM schedule from its parent class code. For example, say that the Four-lane subclass had an empty square beside it, but the Two-lane subclass had a check mark in the square beside it. This indicates that the Four-lane subclass follows the PM schedule of the Primary class, while the Two-lane subclass has its own PM schedule.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a class in the hierarchy, a shortcut menu is displayed. This shortcut menu contains the common commands along with the following special commands:</text></para>
    <list id="6" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Define Its Own PM Setup</text><text styleclass="Bullet L1" translate="true"> — When an inventory type follows the PM schedule of its next-highest ancestor, this command is available. &#160;When available, &#160;this command overrides the PM schedule of the inventory type that is next-highest in the hierarchy and allows you to establish a different PM schedule for the inventory type to which you pointed.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Inherit Parent PM Setup</text><text styleclass="Bullet L1 Last" translate="true"> — When an inventory type has its own PM schedule, this command is available. When available, this &#160;command removes the current PM schedule and assigns the PM schedule of the inventory type that is next highest in the hierarchy to the inventory type to which you pointed.</text></li>
    </list>
  </body>
</topic>
