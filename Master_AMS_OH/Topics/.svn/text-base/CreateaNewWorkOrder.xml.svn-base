﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Create a New Work Order</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Create a New Work Order</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">To create a new work order in the Activities window, follow these steps:</text></para>
    <list id="14" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Open the Activities window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Find the record with the activity that will be done in the work order, right-click the record and then click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">. A dialog box is displayed. The left side of this dialog box lists the attributes needed to create a work order (project template, asset type, activity, and inventory item). The right side of the dialog box lists what may be chosen for the attribute selected on the left.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;Even though the drop-down field on the right is aligned with the first attribute (</text><text styleclass="Text As Shown on Screen" translate="true">Project</text><text styleclass="L1 Indent" translate="true">), it applies to all attributes on the left. The selection of an attribute field on the right determines what is shown in the drop-down list.</text></para>
    <list id="14" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">For the asset type, on the right side of the dialog box, click the appropriate choice for the attribute.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Click </text><text styleclass="Command" translate="true">Next</text><text styleclass="Bullet L1" translate="true">. Your selection appears in the </text><text styleclass="Text As Shown on Screen" translate="true">Asset Type</text><text styleclass="Bullet L1" translate="true"> field and the next attribute is selected (and denoted by highlighting).</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Repeat steps 3 and 4 for the remaining attributes.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;Since the attributes are hierarchical (that is, what you select for an attribute determines what may be selected for the next attribute), if only a single attribute could be selected, the system will automatically assign that attribute.</text></para>
    <list id="14" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Once you have made the selections for all attributes, click </text><text styleclass="Command" translate="true">OK</text><text styleclass="Bullet L1" translate="true">. The dialog box closes and the new work order is shown in the Activities window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Check the </text><text styleclass="Text As Shown on Screen" translate="true">Duration</text><text styleclass="Bullet L1" translate="true"> field. By default, the start date is forced to be today and the duration is one day. Modify &#160;the duration if necessary. (The system will calculate an end date automatically.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">If a crew will perform this activity, select it from the drop-down list shown in the </text><text styleclass="Text As Shown on Screen" translate="true">Responsible Crew</text><text styleclass="Bullet L1" translate="true"> column.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Click </text><image src="Icon_Save.gif" scale="100.00%" styleclass="Bullet L1 Last"></image><text styleclass="Bullet L1 Last" translate="true"> to save the new work order .</text></li>
    </list>
  </body>
</topic>
