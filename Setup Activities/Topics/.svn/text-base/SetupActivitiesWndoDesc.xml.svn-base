﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Description of the Activities Window</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Description of the Activities Window</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Activities window shows a tree view and a table view of all activities. The tree view is expanded by clicking a plus sign (+) or contracted by clicking a minus sign (–). The right pane shows a table view that provides additional information about each activity including:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Activity Code — This column shows the activity code associated with the activity.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Budget Category — This column shows the budget category associated with the activity. The entries in the drop-down list are configured in the Budget Categories window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Measurement Unit — This column shows the measurement unit for the activity. The entries in the drop-down list are configured in the Units window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Order — This column controls the display order of the activity in the tree view.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Unit Cost — This column shows the cost of the activity per unit of time.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">When you select an activity in the tree view pane, the record corresponding to that code is highlighted and displayed in the table on the right. The converse is also true — selecting a record also selects an activity in the tree view.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a node in the tree-view pane, the system displays a shortcut menu. This menu contains the following command in addition to &#160;the common commands:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Add Branch</text><text styleclass="Bullet L1 Last" translate="true"> — This command adds a new, subordinate node to the tree view and a corresponding row in the list view. </text><link displaytype="text" defaultstyle="true" type="topiclink" href="AddaNewActivity" styleclass="Bullet L1 Last" translate="true">Click here</link><text styleclass="Bullet L1 Last" translate="true"> for the procedure on how to add an activity.</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a record in the table-view pane, the system displays a shortcut menu. This menu contains the following commands in addition to &#160;the common commands:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Add Branch</text><text styleclass="Bullet L1" translate="true"> — This command is the same as found in the tree-view pane (see above).</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Edit Descriptions</text><text styleclass="Bullet L1 Last" translate="true"> — This command is only available for non-parent activities. When available, it displays a new window that shows information about the activity and the stored Performance Guideline with which it is associated. You may then edit the displayed information as desired.</text></li>
    </list>
  </body>
</topic>
