﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Input Parameter Fields</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Input Parameter Fields</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The input parameter fields at the top of the window determine what projects are ranked, for what fiscal year, and which criterion is used for ranking. You may also optionally specify a threshold for ranking that sets a floor for which projects are included in the ranking. The fields are described below: </text></para>
    <list id="35" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1 Last" translate="true">Criteria Threshold Value — The criteria threshold value is an optional input for short-term ranking analysis. The value specified here depends on the Ranking Criteria selected. The specified value sets a floor for which projects are included in the ranking analysis in the Projects pane.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Effective Year — This field contains a drop-down list of years. You select a year from the list to set the year of the projects that will be included in ranking analysis (provided they meet the other input parameter criteria) from the Project Composition window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Project Type — This field contains a drop-down list of project types, which are configured in the Short-term Project Types window. You select one of the project types from the list to specify the type of projects that will be available for ranking in the Projects pane of this window. </text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Ranking Criteria — This field contains a drop-down list of ranking expressions, which are configured in the Short-term Ranking Expressions window. You select one of the expressions from the list to set the criteria for how projects are to be ranked.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">After setting the fields as desired, click the </text><text styleclass="Command" translate="true">Refresh</text><text styleclass="Body Text" translate="true"> button to display in the lower panes the projects in order of decreasing rank and each one&apos;s recommended treatments.</text></para>
  </body>
</topic>
