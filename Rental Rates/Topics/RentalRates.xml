﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Rental Rates</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Rental Rates</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The purpose of the Rental Rates window is to provide average operating unit cost estimates for each equipment class based upon recorded historic usage (miles/hours), fuel cost, repair cost, and depreciation cost data. These unit cost estimates are either operating cost per mile traveled or operating cost per hour operated as set by the equipment class, and can be derived either as averages using data for a particular year or as weighted averages using data across several years.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;This window only contains rental rates estimates. The official charged rental rates are retained in the Equipment Class Code window. There is no link between the rental rates in these two windows. Consequently, if you want to make a rental rate estimate official, you must manually enter that value into the Equipment Class Code window for the appropriate equipment class.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The bases for these average estimates are monthly summaries of historic usage and cost data. These summaries are calculated by month by running the Fill Equipment Expense Universe system job.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;Rental rates cannot be calculated in this window until at least one monthly summary is calculated using the Fill Equipment Expense Universe system job.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Rental Rates window contains the following:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Fiscal Year field — This field at the top of the window provides a drop-down list, from which you select which year&apos;s rental rate estimates are shown in the Rental Rate pane. The drop-down list also shows, for each year, the number of monthly summaries that have been calculated. (These monthly summaries form the bases from which rental rate estimates are derived.)</text></li>
    </list>
    <para style="margin-left:13px; tabstops:27px left ;"><text styleclass="Normal" style="font-size:5pt;" translate="true">&#32;</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">The </text><link displaytype="text" defaultstyle="true" type="topiclink" href="RentalRatesPane" styleclass="Bullet L1" translate="true">Rental Rates</link><text styleclass="Bullet L1" translate="true"> pane.</text></li>
    </list>
    <para style="margin-left:13px; tabstops:27px left ;"><text styleclass="Normal" style="font-size:5pt;" translate="true">&#32;</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">The </text><link displaytype="text" defaultstyle="true" type="topiclink" href="AnnualWeightFactorPane" styleclass="Bullet L1 Last" translate="true">Annual Weight Factor</link><text styleclass="Bullet L1 Last" translate="true"> pane.</text></li>
    </list>
  </body>
</topic>
