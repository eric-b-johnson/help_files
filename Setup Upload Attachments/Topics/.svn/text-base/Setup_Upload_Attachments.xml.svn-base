﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Setup Upload Attachments</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Setup Upload Attachments</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">This window allows you to add one or more attachments to records in any table in a batch manner via a system job. Each record in the Setup Upload Attachments window is a batch record that specifies where the files to be attached reside and where the files are to be placed in the AgileAssets database.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">To upload multiple attachments, perform the following steps:</text></para>
    <list id="1" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the window to which the attachment files will be added, create and add a column for each file to be added. For example, if bridge inspections include the taking of ten photographs, then you would create ten columns for the bridge photographs and add these new columns to the Bridge Inventory window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In each of the new attachment columns, enter the name of the file to be attached. Do not include the path to the file; just enter the actual file name including extension.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Setup Upload Attachments window, create a new record by right-clicking and then clicking </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the appropriate columns of the new record, enter the table and column name of the first new attachment column.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the </text><text styleclass="Text As Shown on Screen" translate="true">File Path</text><text styleclass="Bullet L1" translate="true"> column of the new record, enter the path to the location of the file to be attached. If the file may be located in several different places, you may include multiple paths with each path separated by semicolons.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Repeat steps 3 – 5 for the additional columns added in step 1.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Once a record exists in the Setup Upload Attachments window for each new column and file to be attached, display the System Jobs &gt; Schedules window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Schedules window, find the </text><text styleclass="Text As Shown on Screen" translate="true">Upload Attachments</text><text styleclass="Bullet L1" translate="true"> job. Right-click the record showing this job and then click </text><text styleclass="Command" translate="true">Run Job</text><text styleclass="Bullet L1" translate="true">. The system then attaches all files for each and every record in the Setup Upload Attachments window, replacing any existing attachment files.</text></li>
    </list>
  </body>
</topic>
