﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="mboeneke" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">4-Year Plan - COMPASS Projects</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">4-Year Plan - COMPASS Projects</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Pavement Analyst imports the 4-year plan maintenance projects from COMPASS and preventive maintenance and rehabilitation projects from Design and Construction Information System (DCIS). This window is for COMPASS data. Those projects are placed in a staging table for error checks. </text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Pavement Preservation Branch uses Pavement Analyst to import data from the staging table (whose data sources are COMPASS and DCIS) to populate a Master Work Plan (MWP) table that will represent the 4-year plan. &#160;</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Import 4 Year Plan Data</text></para>
    <list id="131" type="ol" listtype="decimal" formatstring="&#37;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">To import data into this buffer table, place the COMPASS 4-year plan data files in the appropriate FTP location.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Run the import job located at System &gt; Tools &gt; System Job &gt; Schedules &gt; </text><text styleclass="Code Example" translate="true">I-04-FYP-1: Import planned projects from COMPASS to PA</text><text styleclass="Bullet L1" translate="true"> for COMPASS data sources.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">You can see the imported data by navigating to Pavement Mgmnt &gt; Analysis &gt; Network Analysis &gt; 4-Year Plan - COMPASS Staging Table.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">This staging (buffer) table is used for error checks performed by the Pavement Preservation Branch in coordination with the districts. When the error checks are complete, run the job to move the data from the staging table to the permanent tables and the MWP table. This job can be found by navigating to System &gt; Tools &gt; System Job &gt; Schedules and right clicking on the system job </text><text styleclass="Code Example" translate="true">I-04-FYP-1: Move COMPASS Planned Projects to Permanent Tables and MWP</text><text styleclass="Bullet L1" translate="true">.</text></li>
    </list>
    <para styleclass="Notes"><text styleclass="Notes" translate="true">Note: It is important not to run any import jobs after completing edits. The import jobs will override all data in staging if there is a new file on the FTP location. Therefore, the staging tables will not match the permanent tables. Edits should only be done after all new four-year plan files have been imported into Pavement Analyst.</text></para>
  </body>
</topic>
