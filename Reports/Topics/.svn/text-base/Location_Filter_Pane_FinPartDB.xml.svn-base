﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Location Filter Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Location Filter Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Location Filter pane shows the columns that specify a location for the currently selected LRS and any filter criteria applied to a column. (This implies that you may create a filter for each LRS that your system employs. A filter is saved by selected LRS, so when you change the LRS the system will know to use the filter for that selected LRS.)</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click a record in this pane, a shortcut menu is displayed. This menu shows the common commands along with the following special command:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Apply Location Filter for All Tables</text><text styleclass="Bullet L1" translate="true"> — This command displays the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Filter" domain="..\Common Commands\Common Commands.hmxp" styleclass="Bullet L1" translate="true">Filter</link><text styleclass="Bullet L1" translate="true"> dialog box, which allows you to set the filter criteria for the selected location-specifying column. The filter you set here will affect all instances of the column in all tables. Once set, the system will show a description of the filter in the </text><text styleclass="Text As Shown on Screen" translate="true">Filter Description</text><text styleclass="Bullet L1" translate="true"> column. (If you want to set a filter for &#160;a non-location-specifying column in a specific table, use the </text><text styleclass="Command" translate="true">Apply Filter for This Table</text><text styleclass="Bullet L1" translate="true"> command that is found in the right-click shortcut menu launched from the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Tables_FinPartDB" styleclass="Bullet L1" translate="true">Tables</link><text styleclass="Bullet L1" translate="true"> pane.)</text></li>
    </list>
  </body>
</topic>
