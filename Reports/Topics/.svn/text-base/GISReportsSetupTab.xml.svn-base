﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">GIS Reports Setup Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">GIS Reports Setup Tab</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">You use the Setup tab to edit or set the parameters for the GIS report. The Setup tab contains the following:</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Filter</text><text styleclass="Bullet L1" translate="true"> — This button displays the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Filter" domain="..\Common Commands\Common Commands.hmxp" styleclass="Bullet L1" translate="true">Filter</link><text styleclass="Bullet L1" translate="true"> dialog box, which you may use to restrict the data used in the report.</text></li>
    </list>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">As of Date — This field allows you to use data from a particular time in the past (rather than current data) for the report. Note: This will only work if the tables on which the report is based are configured to support temporality.</text></li>
    </list>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Color By pane — The Color By pane lists all columns that are in the table on which the GIS report is based. You may select one (and only one) of these columns to display in the GIS report by selecting the check box next to the name of the column. The system then assigns colors to the data from this column and overlays the selected layer with the color that represents the data value along each route in the map.</text></li>
    </list>
  </body>
</topic>
