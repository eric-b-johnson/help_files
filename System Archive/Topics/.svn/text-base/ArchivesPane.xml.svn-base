﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Archives Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Archives Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Archives pane shows all defined archive sets in the database. An archive set is composed of the archive actions indicated in the Archive Details pane.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click an archive set, the system displays a shortcut menu. This menu contains the common commands along with the following special commands:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Archive</text><text styleclass="Bullet L1" translate="true"> — This command initiates the archive actions for the highlighted archive set. For archive sets with multiple detail records, the archive actions are performed in the order, from lowest to highest, indicated in the </text><text styleclass="Text As Shown on Screen" translate="true">Order</text><text styleclass="Bullet L1" translate="true"> column of the Archive Details pane.</text></li>
    </list>
    <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">Note: &#160;If you attempt to use this command to archive data previously archived, the system will warn you that this will destroy the previously archived data. You may then choose to proceed with the archive or abort the process.</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Restore</text><text styleclass="Bullet L1 Last" translate="true"> — This command moves the data specified in the highlighted archive set to the user-accessible portion of the database.</text></li>
    </list>
  </body>
</topic>
