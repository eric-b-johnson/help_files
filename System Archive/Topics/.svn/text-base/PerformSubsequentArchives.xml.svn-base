﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">How to Perform Subsequent Archives</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">How to Perform Subsequent Archives</text></para>
    </header>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">After initially archiving data, additional archiving of the same data will destroy the data previously archived. You should, therefore, not perform subsequent archives without careful consideration of the consequences nor without assistance from AgileAssets. If you do decide to proceed with a subsequent archive, follow these steps:</text></para>
    <list id="2" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Display the System Archive window.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Archives pane, click the desired archive set to select it.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Write down all information shown in the Archive Details pane for the selected archive set. (Alternatively, print the screen to capture this information.)</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Archives pane, right-click and then click </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Bullet L1" translate="true">. A new record is added to the pane.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Archive Details pane, using the information acquired in step 3, duplicate the details of the initial archive by inserting the appropriate number of records and entering the data. Also adjust the </text><text styleclass="Text As Shown on Screen" translate="true">Archive_WC</text><text styleclass="Bullet L1" translate="true"> column to accurately define the historic data to be archived.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Once the Archive Details pane is configured, write down all information shown in this pane. (Alternatively, print the screen to capture this information.)</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Label the information gathered in step 3 and in step 6 so it is clear which information is for which archive set (initial or duplicate) and then send the information to AgileAssets for evaluation.</text></li>
    </list>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">AgileAssets will review the information and then indicate whether you may proceed with the subsequent archive or whether additional refinement of the duplicate archive set is necessary. Once AgileAssets approves the duplicate archive set and explicitly instructs you to proceed:</text></para>
    <list id="3" type="ol" listtype="decimal" formatstring="&#37;&#48;&#58;&#115;&#46;" format-charset="DEFAULT_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:&apos;Book Antiqua&apos;; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Copy the entire Oracle database to a test schema and retain a copy of the dump file.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Archive pane of the test schema, right-click the new archive set and then click </text><text styleclass="Command" translate="true">Archive</text><text styleclass="Bullet L1" translate="true">. The system will begin performing the archive actions.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Once the archive operation completes, check the system as a user to see that everything works. If everything works as expected, you may now perform the archive in the production schema. If problems are found, consult with AgileAssets for assistance in resolving them.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">In the Archive pane of the production schema, right-click the new archive set and then click </text><text styleclass="Command" translate="true">Archive</text><text styleclass="Bullet L1" translate="true">. The system will begin performing the archive actions.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Once the archive operation completes, check the system as a user to see that everything works. If problems are found, replace the production schema with the contents of the dump file and advise AgileAssets to evaluate the situation.</text></li>
    </list>
  </body>
</topic>
