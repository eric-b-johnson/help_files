﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Archive Details Pane</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Archive Details Pane</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Archive Details pane defines all tables and the portion of data per table that will be archived when the Archive command is activated for the selected archive set in the Archive pane. Each detail record defines the data archived for a single table. To prevent the loss of data, several detail records may be needed for a single archive operation and the order that these tables are archived must also be set appropriately. The order is set in the </text><text styleclass="Text As Shown on Screen" translate="true">Order</text><text styleclass="Body Text" translate="true"> column, and the archive actions are performed starting with the lowest-ranked table.</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Note: &#160;If a table is included in an archive, then all tables that have a foreign key to that table must also be included in the archive with a lower value.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Archive Details pane contains the following columns:</text></para>
    <list id="88" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Archive Count — This read-only column indicates the number of records archived for the table associated with the detail record.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Archive_WC — This column contains the Oracle WHERE statement that indicates what portion of the table&apos;s data is to be archived. This statement typically defines a time period for which data will be archived. For example, &quot;archive all data more than 3 years old&quot; would be translated into DATE_WORK &lt; SYSDATE – 365*3 for the Labor Day Cards table.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Order — This column indicates the order in which each table is archived. Tables are archived in order of lowest to highest. Remember that tables with foreign keys must have lower numbers than their parent table.</text></li>
      <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Table Name — This column shows the name of the table being archived.</text></li>
    </list>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">When you right-click this pane, the common commands are available. However, the </text><text styleclass="Command" translate="true">Insert</text><text styleclass="Body Text" translate="true"> command works slightly different than in other windows. It not only inserts a new record in the Archive Details pane, but also displays a window that contains a drop-down list of all table names so you may select the appropriate table for the new detail record.</text></para>
  </body>
</topic>
