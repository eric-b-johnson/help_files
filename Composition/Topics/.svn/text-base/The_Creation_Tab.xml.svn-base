﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">The Creation Tab</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">The Creation Tab</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Creation tab is where you configure the various parameters for an analysis. (You may locate an analysis by entering its ID number in the field above this pane and then clicking </text><text styleclass="Command" translate="true">Find Scenario</text><text styleclass="Body Text" translate="true">. Alternately, you may use the navigation buttons to find a particular analysis.) This pane may also be used to create new analysis scenario definitions or delete an analysis scenario that you no longer need.</text></para>
    <para styleclass="Internal Heading"><text styleclass="Internal Heading" translate="true">The Scenarios Pane</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The Scenarios pane lists the analysis parameters. It contains </text><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="true" translate="true" styleclass="Body Text Before List"><caption translate="true"><![CDATA[the following fields:]]></caption></toggle></para>
    <para styleclass="Body Text Before List"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <list id="312" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Analysis Length — This is the number of years in the analysis period.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Analysis Scope — This read-only field shows the data elements that are included in the analysis. The scope is set with the </text><text styleclass="Command" translate="true">Edit Scope</text><text styleclass="Bullet L1" translate="true"> command.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Comments — Optionally, you may include information in this field to describe the analysis.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Has Results — This check box indicates whether the impact analysis has already been performed. When a check mark is not displayed, then the results information shown in the other tabs is irrelevant or not available.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Scenario Name — This is a short description of the analysis. This description should be informative to other users.</text></li>
            <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Scenario Number — This is a sequential number automatically assigned to identify the analysis. It cannot be edited. (This is the ID number to be used with the </text><text styleclass="Command" translate="true">Find Scenario</text><text styleclass="Bullet L1" translate="true"> button.)</text></li>
            <li styleclass="Bullet L1 Last"><text styleclass="Bullet L1 Last" translate="true">Year of Condition Data — The year you enter in this field is the year when condition data was collected, which sets the first year of analysis as this year plus one. (In most cases, the Network Master File contains only a single year&apos;s data and the value in this field sets the year of that data. For those cases where the Network Master File contains data from multiple years, the year in this field configures what records will be used for analysis.)</text></li>
          </list>
        </td>
      </tr>
    </table></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">When you right-click the Scenarios pane, the system displays a shortcut menu. This menu contains the common commands along with the </text><toggle type="dropdown" print-expanded="true" help-expanded="false" defaultstyle="true" translate="true" styleclass="Body Text Before List"><caption translate="true"><![CDATA[following special commands:]]></caption></toggle></para>
    <para styleclass="Body Text Before List"><table styleclass="Default" rowcount="1" colcount="1">
      <tr style="vertical-align:top">
        <td>
          <list id="313" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Copy Scenario</text><text styleclass="Bullet L1" translate="true"> — This command allows you to copy the parameters of the selected analysis, which may then be pasted into another analysis to create a new one.</text></li>
          </list>
          <list id="314" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1"><text styleclass="Command" translate="true">Edit Scope</text><text styleclass="Bullet L1" translate="true"> — This command allows you to set the scope for the impact analysis. Analysis scope allows you to limit the data elements included in the analysis. When you execute this command, the application displays a new window. This window shows the variables that may be utilized to limit the scope of the analysis. You use this window in the same way as the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Filter" domain="..\Common Commands\Common Commands.hmxp" styleclass="Bullet L1" translate="true">Filter</link><text styleclass="Bullet L1" translate="true"> window. (The variables are designated (and can be changed) in the </text><link displaytype="text" defaultstyle="true" type="topiclink" href="Setup_Tradeoff_Analysis_Columns" domain="..\Setup Trade-off Analysis Columns\Setup Trade-off Analysis Columns.hmxp" styleclass="Bullet L1" translate="true">Setup Columns in Trade-off Analysis</link><text styleclass="Bullet L1" translate="true"> window.)</text></li>
          </list>
          <para styleclass="L1 Indent"><text styleclass="L1 Indent" translate="true">After setting the analysis scope, the selected elements are shown in the </text><text styleclass="Text As Shown on Screen" translate="true">Analysis Scope</text><text styleclass="L1 Indent" translate="true"> field.</text></para>
          <list id="315" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1 Last" style="font-family:Symbol; font-size:10pt; color:#000000;">
            <li styleclass="Bullet L1 Last"><text styleclass="Command" translate="true">Run Scenario</text><text styleclass="Bullet L1 Last" translate="true"> — This command executes the displayed impact analysis.</text></li>
          </list>
        </td>
      </tr>
    </table></para>
    <para styleclass="Internal Heading"><text styleclass="Internal Heading" translate="true">The Influence: Reporting Functions Pane</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Influence: Reporting Functions pane shows the variables (&quot;reporting functions&quot;) that may be affected by the analysis (that is, impacted by the projects inserted or copied into the Projects pane). The reporting functions listed here will be those shown in the Influence (Results) tab. </text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">If desired, you may subdivide a particular reporting function by right-clicking its record and then selecting </text><text styleclass="Command" translate="true">Activate Constraint Subdivisions</text><text styleclass="Body Text" translate="true">. After selecting this command, the system displays a new window with the available subdivisions for the reporting function (as configured in the Setup Constraint Subdivisions window). You select the desired subdivision and then the system will add additional records to this pane for the reporting function&apos;s subdivisions.</text></para>
  </body>
</topic>
