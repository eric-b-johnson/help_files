﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Optimization Analysis Window Description</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Optimization Analysis Window Description</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">The Optimization Analysis window is where all optimizations are performed. In this window you can define and run multiple optimizations, each having different settings, budgets, analysis periods, and so forth. The results from each optimization are stored separately and can be reviewed in this and other windows throughout the system.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The window provides the following tabs that allow you to switch between different components of project optimization:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><link displaytype="text" defaultstyle="true" type="topiclink" href="SetupTab_OptAnal" styleclass="Bullet L1" translate="true">Setup</link><text styleclass="Bullet L1" translate="true"> — This tab is where all parameters for an optimization are established.</text></li>
      <li styleclass="Bullet L1"><link displaytype="text" defaultstyle="true" type="topiclink" href="ResultsTab_OptAnal" styleclass="Bullet L1" translate="true">Results</link><text styleclass="Bullet L1" translate="true"> — This tab shows the optimal recommended work plan.</text></li>
      <li styleclass="Bullet L1"><link displaytype="text" defaultstyle="true" type="topiclink" href="ConstrResults__ProjOpt" styleclass="Bullet L1" translate="true">Constr. Results</link><text styleclass="Bullet L1" translate="true"> — This tab shows the predicted values of each constraint compared to the input constraint value. This allows you to identify the constraints that have controlled the analysis results.</text></li>
      <li styleclass="Bullet L1"><link displaytype="text" defaultstyle="true" type="topiclink" href="ReportTab_OptAnal" styleclass="Bullet L1" translate="true">Report</link><text styleclass="Bullet L1" translate="true"> — This tab shows the constraints selected in the Reporting Functions pane of the Setup tab and the value of each constraint after optimization.</text></li>
      <li styleclass="Bullet L1 Last"><link displaytype="text" defaultstyle="true" type="topiclink" href="Summary_Reports_OptAnal" styleclass="Bullet L1 Last" translate="true">Summary Reports</link><text styleclass="Bullet L1 Last" translate="true"> — When an optimization analysis has results, this tab becomes available. It provides a less data-intensive way to view graphs using data from the selected optimization analysis. It also allows you to compare two different optimization analyses.</text></li>
    </list>
  </body>
</topic>
