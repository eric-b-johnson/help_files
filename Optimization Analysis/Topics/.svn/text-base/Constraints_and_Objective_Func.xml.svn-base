﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="tbeamish" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Constraints and Objective Function</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Constraints and Objective Function</text></para>
    </header>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">Each optimization analysis has one objective, which is essentially the goal of the optimization analysis. Each analysis also has one or more constraints. These serve as the mechanism to halt analysis during a particular year of the analysis period. Optionally, you may also configure different constraint levels, either per year or per additional user-defined &quot;segregating&quot; variables such as functional class and district. A wide variety of constraint and segregating variables are available, and they may all be configured from within an AgileAssets application (for example, via the PMS Columns Used in Analysis and Setup Constraint Subdivisions windows).</text></para>
    <para styleclass="Body Text"><text styleclass="Body Text" translate="true">All constraint variables as well as the objective may be aggregated by one of the following methods: percentage above threshold; total; or weighted average. Condition variables are typically aggregated as weighted average or percentage above threshold; the Benefit variable must be aggregated as weighted average; and the Cost variable must be aggregated as a total.</text></para>
    <para styleclass="Body Text Before List"><text styleclass="Body Text Before List" translate="true">The following objective functions are available in multi-constraint optimization:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Bullet L1" style="font-family:Symbol; font-size:10pt; color:#000000;">
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Minimize treatment cost.</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Maximize condition (Benefit, RSL, or Performance Indices).</text></li>
      <li styleclass="Bullet L1"><text styleclass="Bullet L1" translate="true">Maximize the percentage of the network above a given threshold (RSL &gt; x or Performance Index &gt; x).</text></li>
    </list>
  </body>
</topic>
